# Search For A Star 2018

This repository contains Miguel Fernandez's submission for Search for a Star 2018.

All work is propietary and developed by Miguel Fernandez Arce except "Assets/Packages" folder and the vehicles "Assets/Entities/Meshes".

This project was entirely developed and designed in the range of 1 and a half months.


### License
Copyright 2017-2018 Miguel Fernandez Arce.
All rights reserved.