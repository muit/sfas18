﻿using UnityEngine;
using System.Collections.Generic;
using Crab;

public class Cache : MonoBehaviour {
    [Header("Character Prefabs")]
    public List<SFASPlayerController> vehicles;

    [Header("Prefabs")]
    public TargetMarker targetMarkerPrefab;
    public PuntuationMarker puntuationMarkerPrefab;

    //[Header("Object Prefabs")]

    [Header("References")]
    public new BaseControlledCamera camera;

    GameInstance _gameInstance;

    public GameInstance gameInstance {
        get {
            return _gameInstance ? _gameInstance : _gameInstance = FindObjectOfType<GameInstance>();
        }
    }


    //Singleton
    private static Cache instance;
    public static Cache Get
    {
        get {
            return instance? instance : instance = FindObjectOfType<Cache>();
        }
        private set { instance = value; }
    }
}
