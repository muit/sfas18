﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Crab
{
    public class GameInstance : MonoBehaviour
    {
        // Static singleton property
        public static GameInstance Get { get; private set; }

        void OnEnable()
        {
            // Check if there are any other instances conflicting
            if (Get != null)
            {
                Destroy(gameObject);
            }
            Get = this;

            DontDestroyOnLoad(gameObject);
        }

        void Start()
        {

            BeforeConstruct();
            Construct();
        }


        //Events
        protected virtual void BeforeConstruct() { }
        protected virtual void Construct() { }

        protected virtual void Update() { }


        static int scenesBeingLoaded = 0;

        public delegate void LevelLoadEvent();
        public static event LevelLoadEvent OnLoadStarted;
        public static event LevelLoadEvent OnLoadFinished;

        static public Scene LoadScene(int sceneId, LoadSceneMode mode = LoadSceneMode.Single, bool once = true, UnityAction<Scene> callback = null)
        {
            Scene scene = SceneManager.GetSceneAt(sceneId);

            if (once && scene.IsValid() && scene.isLoaded)
            {
                if (callback != null)
                    callback(scene);

                return scene;
            }
            else
            {
                AsyncOperation operation = SceneManager.LoadSceneAsync(sceneId, mode);
                scene = SceneManager.GetSceneAt(sceneId);

                PostLoadScene(operation, callback, "", sceneId);
                return scene;
            }
        }

        static public Scene LoadScene(string sceneName, LoadSceneMode mode = LoadSceneMode.Single, bool once = true, UnityAction<Scene> callback = null)
        {
            Scene scene = SceneManager.GetSceneByName(sceneName);

            if (once && scene.IsValid() && scene.isLoaded)
            {
                if (callback != null)
                    callback(scene);

                return scene;
            }
            else
            {
                AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName, mode);

                PostLoadScene(operation, callback, sceneName);
                return scene;
            }
        }

        static void PostLoadScene(AsyncOperation operation, UnityAction<Scene> callback = null, string sceneName = "", int sceneId = -1)
        {
            ++scenesBeingLoaded;
            if (scenesBeingLoaded == 1)
            {
                if(OnLoadStarted != null)
                    OnLoadStarted();
            }

            operation.completed += (AsyncOperation) =>
            {
                --scenesBeingLoaded;

                if (scenesBeingLoaded <= 0)
                {
                    scenesBeingLoaded = 0;

                    if (OnLoadFinished != null)
                        OnLoadFinished();
                }
                
                Scene scene = SceneManager.GetSceneByName(sceneName);
                if (scene == null)
                    scene = SceneManager.GetSceneAt(sceneId);

                if (callback != null)
                    callback(scene);
            };
        }


        static public void UnloadScene(int scene)
        {
            SceneManager.UnloadSceneAsync(scene);
        }
        static public void UnloadScene(string scene)
        {
            SceneManager.UnloadSceneAsync(scene);
        }
        static public void UnloadScene(Scene scene)
        {
            if (scene != null && scene.IsValid() && scene.isLoaded)
            {
                SceneManager.UnloadSceneAsync(scene);
            }
        }
    }
}
