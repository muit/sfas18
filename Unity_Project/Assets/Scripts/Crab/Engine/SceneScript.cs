﻿using UnityEngine;
using Crab.Events;
using System.Collections.Generic;

namespace Crab
{
    public class SceneScript : MonoBehaviour
    {
        public string CustomGameInstance = "GameInstance";

        
        System.Type GameInstanceClass = typeof(GameInstance);

        // Static singleton property
        private static List<SceneScript> sceneScripts = new List<SceneScript>();


        public static SceneScript Get(GameObject context) {
            if (!context)
            {
                //Return first scene if there is no context
                if (sceneScripts.Count > 0)
                    return sceneScripts[0];
                else
                    return null;
            }

            return sceneScripts.Find((SceneScript script) =>
            {
                return script && script.gameObject.scene == context.scene;
            });
        }
        public static T Get<T>(GameObject context) where T : SceneScript { return Get(context) as T; }

        public static SceneScript Get(Component context) { return Get(context.gameObject); }
        public static T Get<T>(Component context) where T : SceneScript { return Get(context.gameObject) as T; }


        protected virtual void OnEnable() {
            if (CustomGameInstance.Length > 0 && CustomGameInstance != "GameInstance")
            {
                System.Type NewClass = System.Type.GetType(CustomGameInstance);
                if (NewClass != null)
                    GameInstanceClass = NewClass;
            }

            if (!FindObjectOfType<GameInstance>())
            {
                //Create game Instance if no one is found
                new GameObject("Game Instance", GameInstanceClass);
            }
        }

        protected virtual void Awake()
        {
            players = new List<PlayerController>();
            
            // Check if there are any other instances conflicting. Remove if it is the case
            if (Get(this) != null)
            {
                Destroy(gameObject);
            }

            sceneScripts.Add(this);
        }

        [System.NonSerialized]
        public ESpawn spawn;
        [System.NonSerialized]
        public List<PlayerController> players;

        void Start()
        {
            spawn = FindObjectOfType<ESpawn>();
            BeforeGameStart();

            if (players == null || players.Count <= 0)
            {
                players = new List<PlayerController>(FindObjectsOfType<PlayerController>());
            }

            OnGameStart(players);
        }


        //Events
        protected virtual void BeforeGameStart() { }

        protected virtual void OnGameStart(List<PlayerController> localPlayers) { }

        protected virtual void Update() { }

        public virtual void PlayerDied(PlayerController player, Entity killer) { }
        
        protected virtual void OnDestroy()
        {
            sceneScripts.Remove(this);
        }


        //Handlers

        public bool RegistryPlayer(PlayerController player)
        {
            if(!players.Contains(player))
            {
                players.Add(player);
                return true;
            }
            return false;
        }

        public bool UnregistryPlayer(PlayerController player)
        {
            return players.Remove(player);
        }

        public static Entity SpawnEntity(Transform parent, Entity prefab, Vector3 position, Quaternion rotation, bool controlled = true)
        {
            if (prefab && parent)
            {
                EntityController controller = null;
                if (controlled && prefab.controllerPrefab) {
                    controller = Instantiate(prefab.controllerPrefab, position, rotation, parent);
                }

                Entity spawnedEntity = Instantiate(prefab, position, rotation);

                if (controller != null)
                {
                    controller.SetControlledEntity(spawnedEntity);
                    controller.name = spawnedEntity.name + "_Controller";
                }

                return spawnedEntity;
            }
            return null;
        }

        public PlayerController GetPlayer(int id)
        {
            if (players.Count <= id)
                return null;
            return players[id];
        }

        public int GetPlayerId(PlayerController player)
        {
            return players.IndexOf(player);
        }
    }
}