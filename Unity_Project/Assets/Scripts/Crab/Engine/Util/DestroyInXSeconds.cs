﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Crab.Utils
{
    public class DestroyInXSeconds : MonoBehaviour
    {

        public bool activated = true;
        public float seconds = 3.0f;

        private Delay delay;

        // Use this for initialization
        void Start()
        {
            if (activated)
                ActivateDestruction();
        }

        // Update is called once per frame
        void Update()
        {
            if (delay != null && delay.Over())
            {
                InstantDestroy();
            }
        }

        public void ActivateDestruction()
        {
            delay = new Delay(seconds, true);
        }

        public void InstantDestroy()
        {
            Destroy(gameObject);
        }
    }
}
