﻿using UnityEngine;
using System.Collections.Generic;

namespace Crab.Utils
{
    public class Delay
    {
        private float length;
        private float endTime;
        private bool started;

        public float Length { get { return length; } }

        
        public Delay(float seconds, bool start = true)
        {
            length = seconds;
            if (start)
                Start();

            started = start;
        }

        public bool Over()
        {
            if (!started)
                return false;

            return Time.time >= endTime;
        }

        public float TimeRemaining() {
            return IsStarted() ? endTime - Time.time : 0;
        }
        public float CofRemaining()
        {
            if (length <= 0)
                return 0;
            return IsStarted() ? (endTime - Time.time)/length : 0;
        }

        //Start the delay with x seconds
        public void Start(float newLength = -1)
        {
            if (newLength != -1) {
                length = newLength;
            }


            started = true;
            endTime = length + Time.time;
        }

        public void Reset()
        {
            started = false;
        }

        public void End()
        {
            started = true;
            endTime = 0;
        }

        public bool IsStarted() {
            return started;
        }
    }
}