﻿using UnityEngine;
using Crab.Entities;

namespace Crab
{
    public class PlayerController : EntityController
    {
        private CBaseMovement movement;

        private int playerId;
        public int PlayerId {
            get {
                return playerId >= 0 ? playerId : playerId = SceneScript.Get(this).GetPlayerId(this);
            }
        }
        
        protected BaseControlledCamera Cam {
            get {
                return Cache.Get.camera;
            }
        }


        protected virtual void Awake()
        {
            playerId = -1;
        }

        protected override void OnPossess()
        {
            movement = Me.Movement;

            //Registry player if he is not already
            SceneScript scene = SceneScript.Get(this);
            if (scene)
            {
                scene.RegistryPlayer(this);
            }
        }

        protected override void JustDespawned()
        {
            //Registry player if he is not already
            SceneScript scene = SceneScript.Get(this);
            if (scene)
            {
                scene.UnregistryPlayer(this);
            }
        }

        protected override void Update()
        {
            movement.Move(Input.GetAxis("Horizontal_P" + PlayerId), Input.GetAxis("Vertical_P" + PlayerId));
        }

        public override void JustDied(Entity killer)
        {
            base.JustDied(killer);
            SceneScript.Get<GameScene>(this).PlayerDied(this, killer);
        }
    }
}