﻿using UnityEngine;
using UnityEngine.Events;
using Crab.Utils;


namespace Crab
{
    [System.Serializable]
    public class DamageEvent : UnityEvent<int, Entity> { }

    [System.Serializable]
    public class DeadEvent : UnityEvent<Entity> { }
    
    [DisallowMultipleComponent]
    public class EntityController : MonoBehaviour {

        public DamageEvent OnAnyDamage;
        public DeadEvent OnDead;
        
        protected Entity me;
        public Entity Me {
            get {
                return me? me : me = GetComponent<Entity>();
            }
        }

        protected EventsMap<int> events = new EventsMap<int>();

        void Start() {
            events.Father = this;
        }

        void OnDestroy()
        {
            JustDespawned();
        }


        //Reusable Methods
        protected virtual void OnPossess() { }
        protected virtual void JustDespawned() { }

        protected virtual void EnterCombat(Entity target) { }

        protected virtual void FinishCombat(Entity enemy) { }

        protected virtual void Update()
        {
            events.Update();
        }

        public virtual void JustDied(Entity killer)
        {
            if (OnDead != null)
            {
                OnDead.Invoke(killer);
            }
        }

        protected virtual void JustKilled(Entity victim) { }

        public virtual void AnyDamage(int damage, Entity damageCauser, DamageType damageType) {
            if (OnAnyDamage != null)
            {
                OnAnyDamage.Invoke(damage, damageCauser);
            }
        }


        public void SetControlledEntity(Entity entity) {

            if (!entity)
                return;

            if (Me && Me.controller)
            {
                Me.controller.StopControlling();
            }

            me = entity;
            Me.controller = this;
            
            OnPossess();
        }

        public void StopControlling()
        {
            if (me)
            {
                me.controller = null;
            }

            me = null;
        }
    }
}