﻿
using UnityEngine;
using Crab;
using Crab.Utils;
using Crab.Entities;


namespace Crab.Entities
{
    public class CProjectileWeapon : CWeapon
    {
        public Projectile projectilePrefab;
        public int bulletPoolSize = 40;

        ProjectilePool pool;

        protected override void Start()
        {
            base.Start();

            pool = new ProjectilePool(bulletPoolSize);
        }

        protected override bool CanFire(Entity target) {
            return base.CanFire(target);
        }

        protected override void OnFire() {
            base.OnFire();

            Missile missile = pool.Create(projectilePrefab, fireLocation.position, fireLocation.rotation, Me) as Missile;

            //If fired projectile is a missile try to set its target
            if (missile && target)
            {
                missile.Target = target.transform;
            }

            FinishFiring();
        }

        protected override void AfterFire() {}
    }
}
