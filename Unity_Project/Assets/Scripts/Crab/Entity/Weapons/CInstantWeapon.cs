﻿
using UnityEngine;
using Crab;
using Crab.Utils;
using Crab.Entities;


namespace Crab.Entities
{
    public class CInstantWeapon : CWeapon
    {

        protected override bool CanFire(Entity target) {
            return base.CanFire(target);
        }

        protected override void OnFire() { base.OnFire(); }

        protected override void AfterFire() {}
    }
}
