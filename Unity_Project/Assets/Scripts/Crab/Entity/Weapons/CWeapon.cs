﻿
using UnityEngine;
using Crab;
using Crab.Utils;
using Crab.Entities;
using UnityEngine.Events;

namespace Crab.Entities
{
    [System.Serializable]
    public class FireEvent : UnityEvent { }

    [DisallowMultipleComponent]
    public class CWeapon : EntityComponent
    {
        [Header("Firing")]
        public Transform fireLocation;
        public float fireRate = 3;
        public bool continousFire = false;

        [Header("Magazine")]
        [Tooltip("Amount of bullets ready to be fired (0 = no reload needed)")]
        public int magazineSize = 0;
        public int magazineContent = 0;
        public float reloadTime = 0.8f;
        public bool autoReload = false;
        public bool continueFiringAfterReload = false;

        [Header("General")]
        [Tooltip("Amount of bullets ready to be reloaded (-1 = infinite)")]
        public int ammunition = -1;

        public FireEvent OnFireEvent;

        [System.NonSerialized]
        protected Entity target;

        Delay fireDelay;
        Delay reloadDelay;
        AudioSource fireLocationAudio;


        private bool firing = false;
        public bool IsFiring {
            get { return firing; }
        }

        public bool UsesMagazines {
            get { return magazineSize > 0; }
        }


        protected virtual void Start()
        {
            fireDelay = new Delay(0.5f, false);
            reloadDelay = new Delay(reloadTime, false);

            if (!fireLocation)
                fireLocation = transform;

            if (fireLocation)
                fireLocationAudio = fireLocation.GetComponent<AudioSource>();
        }


        public void Fire(Entity target = null)
        {
            this.target = target;

            if (NeedsReload() && autoReload) {
                Reload();
            }
            else if (CanFire(target))
            {
                firing = true;

                if (magazineSize > 0) {
                    --magazineContent;
                }

                OnFire();
                OnFireEvent.Invoke();

                if (NeedsReload() && autoReload)
                {
                    Reload();
                }
                else
                {
                    fireDelay.Start(1f / fireRate);
                }
            }
        }

        public void FinishFiring() {
            if(!IsFiring)
                return;

            firing = false;
            fireDelay.Start(1f / fireRate);

            AfterFire();
        }

        public bool Reload() {
            if (NeedsReload() && !reloadDelay.IsStarted())
            {
                reloadDelay.Start(reloadTime);
                return true;
            }
            return false;
        }

        private void ApplyReload()
        {
            UnityEngine.Debug.Log("Reloading...");

            //Do we use infinite ammo?
            if (ammunition < 0)
            {
                magazineContent = magazineSize;
            }
            else
            {
                //In case we don't have enough ammunition...
                int readyBullets = Mathf.Min(magazineSize, ammunition);

                ammunition -= readyBullets;
                magazineContent = readyBullets;
            }

            OnReload();

            if (IsFiring)
            {
                if (!continueFiringAfterReload)
                {
                    FinishFiring();
                }
                else
                {
                    Fire();
                }
            }
        }


        protected virtual void Update() {
            if (IsReloading())
            {
                if (reloadDelay.Over())
                {
                    reloadDelay.Reset();

                    //Apply reload after delay
                    ApplyReload();
                }
            }
            else if (IsFiring && fireDelay.Over())
            {
                if (continousFire)
                {
                    fireDelay.Reset();
                    Fire();
                }
            }
        }

        
        protected virtual bool CanFire(Entity target) {
            return !IsFiring && fireDelay != null && (fireDelay.Over() || !fireDelay.IsStarted()) && !NeedsReload() && !IsReloading();
        }

        protected virtual bool NeedsReload() {
            return UsesMagazines && magazineContent <= 0 && (ammunition > 0 || ammunition == -1);
        }

        bool IsReloading()
        {
            return reloadDelay != null && reloadDelay.IsStarted();
        }


        // EVENTS

        protected virtual void OnFire() {
            if (fireLocationAudio)
            {
                fireLocationAudio.Stop();
                fireLocationAudio.Play();
            }
        }

        protected virtual void OnReload() {}

        protected virtual void AfterFire() {}
    }
}