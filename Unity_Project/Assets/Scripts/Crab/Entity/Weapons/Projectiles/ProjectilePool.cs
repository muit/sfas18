﻿using UnityEngine;
using System.Collections.Generic;
using Crab;

public class ProjectilePool : List<Projectile>
{
    public readonly int size;

    public ProjectilePool(int size) : base()
    {
        this.size = size;
    }

    public Projectile Create(Projectile prefab, Vector3 position, Entity owner = null)
    {
        return Create(prefab, position, Quaternion.identity, owner);
    }

    public Projectile Create(Projectile prefab, Vector3 position, Quaternion rotation, Entity owner = null)
    {
        Projectile proj;
        if (Count < size || size == 0)
        {
            //Create a new bullet
            proj = Projectile.Instantiate(prefab, position, rotation);

            //Assign owner
            proj.owner = owner;
            Add(proj);
        }
        else
        {
            //Limit exceeded. Reuse bullet
            proj = this[0];
            RemoveAt(0);

            if (!proj) {
                //Create a new bullet if the actual one was destroyed
                proj = Projectile.Instantiate(prefab, position, rotation);
            } else {
                proj.transform.position = position;
                proj.transform.rotation = rotation;
                proj.OnStart();
            }
            //Assign owner
            proj.owner = owner;

            Add(proj);
        }
        return proj;
    }
}
