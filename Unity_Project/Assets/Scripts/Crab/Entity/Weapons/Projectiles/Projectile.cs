﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Crab;
using Crab.Utils;


public enum ProjectileType {
    Bullet,
    Missile
}

[Serializable]
public class ProjectilePreset<T>
    where T : Projectile
{
    public Transform fireLocation;
    public T prefab;
    public int fireRate = 4;
}

public class Projectile : MonoBehaviour {

    [NonSerialized]
    public Entity owner;

    public int damage = 5;
    public float initialForce = 300;
    public float lifeTime = 5f;


    public ParticleSystem destroyEffectPrefab;
    public float particleRate = 1.0f;
    public Vector3 particleScale = Vector3.one;

    public float collisionImpactImpulse = 300;

    public UnityEvent onHit;

    protected new Rigidbody rigidbody;
    protected Delay destroyDelay;

	// Use this for initialization
	protected virtual void Start () {
        //Setup references
        rigidbody = GetComponent<Rigidbody>();

        //Setup the bullet for projectile pool usage
        OnStart();
        destroyDelay = new Delay((int)(lifeTime * 1000), true);
    }

    // Update is called once per frame
    protected virtual void Update () {
        if (destroyDelay.Over()) {
            //If destroy time has passed, destroy the bullet
            Destroy(gameObject);
        }
	}

    //Reset or secondary start for projectile pool usage
    public virtual void OnStart()
    {
        rigidbody.AddForce(transform.forward * initialForce);

        if (owner)
        {
            owner.OnDestroyEvent += OnOwnerDestroyed;
        }
    }

    private void OnDestroy()
    {
        if (owner)
            owner.OnDestroyEvent -= OnOwnerDestroyed;
    }

    public void OnHit(Collider col) {
        if (col.isTrigger || col.transform.parent == this)
            return;
        
        bool affectedSomething = false;

        //Entities
        Entity entity = Entity.FindIn(col);

        //Is entity and is enemy of our owner?
        if (entity && entity != owner && (!owner || entity.IsEnemyOf(owner)))
        {
            //Apply damage and call events
            entity.Damage(damage, owner);
            affectedSomething = true;
        }

        //Destroyables
        if (!entity || entity != owner)
        {
            //Don't apply to owned destroyables!
            Destroyable destroyable = col.GetComponent<Destroyable>();
            if (!destroyable)
                destroyable = col.GetComponentInParent<Destroyable>();

            if (destroyable)
            {
                destroyable.ApplyDestruction(damage);
                affectedSomething = true;
            }

            // Applying physical impact
            {
                Rigidbody collidedBody = col.GetComponentInParent<Rigidbody>();
                if (collidedBody)
                {
                    Debug.DrawLine(transform.position, transform.position + rigidbody.velocity.normalized, Color.red, 3);
                    collidedBody.AddForce(collisionImpactImpulse * rigidbody.velocity.normalized, ForceMode.Impulse);
                }
            }
        }


        if (affectedSomething)
        {
            if (destroyEffectPrefab && UnityEngine.Random.Range(0.0f, 1.0f) < particleRate)
            {
                ParticleSystem particles = ParticleSystem.Instantiate(destroyEffectPrefab, transform.position, transform.rotation);
                particles.transform.localScale = particleScale;
                particles.Play();
            }
        }

        if (affectedSomething || !entity)
        {
            onHit.Invoke();
            Destroy(gameObject);
        }
    }

    protected virtual void OnOwnerDestroyed()
    {
        if(this && gameObject != null)
            Destroy(gameObject);
    }
}
