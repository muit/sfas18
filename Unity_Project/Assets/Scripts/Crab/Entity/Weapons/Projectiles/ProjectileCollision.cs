﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileCollision : MonoBehaviour {
    void OnTriggerEnter(Collider col)
    {
        if (col.isTrigger || col.transform.parent == this)
            return;

        Projectile parent = GetComponentInParent<Projectile>();


        if (parent) {
            parent.OnHit(col);
        }
    }
}
