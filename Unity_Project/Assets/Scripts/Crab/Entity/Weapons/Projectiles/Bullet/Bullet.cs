
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crab;

[Serializable]
public class BulletPreset : ProjectilePreset<Bullet> { }

public class Bullet : Projectile { }