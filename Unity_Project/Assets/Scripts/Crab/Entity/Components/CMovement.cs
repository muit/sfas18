﻿
namespace Crab.Entities
{
    using UnityEngine;
    using Utils;

    [RequireComponent(typeof(Entity))]

    [DisallowMultipleComponent]
    public class CMovement : CBaseMovement {
        public float speed = 3.5f;
        public float sideSpeed = 3f;
        public float accelerationForce = 200;
        public float rotateSpeed = 15;

        protected override void UpdateAgent() {
            agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
            if (agent)
            {
                agent.speed = speed;
                agent.acceleration = accelerationForce;
            }
        }


        public override void Move(float h, float v) {
            if (Mathf.Abs(h) > 0.01f || Mathf.Abs(v) > 0.01f)
            {
                moveVector = new Vector3(h * sideSpeed, 0, v*speed);
            }
            else
            {
                moveVector = Vector3.zero;
            }
        }

        protected override void MoveUpdate()
        {
            if (characterController)
            {
                characterController.Move((transform.TransformDirection(moveVector) + Physics.gravity) * Time.deltaTime);
            }
            else if (agent)
            {
                agent.Move(transform.TransformDirection(moveVector) * Time.deltaTime);
            }

            //Rotate
            if (viewDirection != Vector3.zero)
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(viewDirection), rotateSpeed * Time.deltaTime);
            }
        }
        
        
        /**
         * Crouch
         */
        private bool crouching;
        public bool canCrouch = true;

        public void StartCrouching() {
            if (canCrouch)
            {
                crouching = true;
                animator.SetBool("Crouch", true);
            }
        }
        public void StopCrouching() {
            crouching = false;
            animator.SetBool("Crouch", false);
        }

        public bool isCrouching {
            get {
                return crouching;
            }
        }


        /**
         * Sprint
         */
        private float sprinting;
        public bool canSprint = true;

        public void StartSprint() {
            if (canSprint)
            {
                sprinting = 1f;
                animator.SetFloat("Sprint", sprinting);
            }
        }

        public void StopSprint() {
            sprinting = 0f;
            animator.SetFloat("Sprint", sprinting);
        }

        public bool isSprinting {
            get {
                return sprinting > 0;
            }
        }


        /**
         * Helpers
         */
        public void ReduceSpeedByPct(float pct) {
            speed     *= 1 - pct / 100;
            sideSpeed *= 1 - pct / 100;

            UpdateAgent();
        }
    }
}