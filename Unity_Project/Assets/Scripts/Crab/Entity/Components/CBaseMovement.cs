﻿
namespace Crab.Entities
{
    using UnityEngine;
    using Utils;

    [RequireComponent(typeof(Entity))]

    [DisallowMultipleComponent]
    public class CBaseMovement : EntityComponent {

        protected Animator animator;
        protected EntityFloor floor;


        //Camera
        [System.NonSerialized]
        public Vector3 viewDirection;
        protected Vector3 moveVector;


        protected UnityEngine.AI.NavMeshAgent agent;
        protected Transform agentTarget;
        protected CharacterController characterController;
        private CharacterController targetCController;

        protected virtual void Awake() {
            //Setup references
            floor = GetComponentInChildren<EntityFloor>();
            animator = GetComponentInChildren<Animator>();
            characterController = GetComponent<CharacterController>();

            UpdateAgent();
        }

        protected virtual void UpdateAgent() {}


        public virtual void Move(float h, float v) {
        }
        public virtual void AIMove(Vector3 position) {
        }
        public virtual void AIMove(Transform target)
        {
        }

        void FixedUpdate() {
            if (!Me.IsAlive())
                return;

            AIUpdate();

            //IsMoving
            if (IsMoving())
            {
                MoveUpdate();
            }
        }

        protected virtual void AIUpdate() {}

        protected virtual void MoveUpdate()
        {}

        public bool IsMoving() {
            return moveVector != Vector3.zero;
        }


        /**
         * Physics
         */
        private bool falling;

        public void StartFalling() {
            if (!falling)
            {
                animator.SetTrigger("StartFalling");
            }
            falling = true;
            animator.SetBool("Falling", falling);
        }

        public void StopFalling() {
            falling = false;
            animator.SetBool("Falling", falling);
        }

        public bool Falling { get { return falling; } }

        public Vector3 Velocity {
            get {
                if (characterController)
                    return characterController.velocity;
                else if (agent)
                    return agent.velocity;
                return Vector3.zero;
            }
        }


        public bool IsVehicle()
        {
            return this is CVehicleMovement;
        }
    }
}