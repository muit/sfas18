﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Crab.Entities
{
    using UnityEngine;
    using Utils;
    
    public class EntityComponent : MonoBehaviour {
        
        private Entity me;
        public Entity Me
        {
            get {
                if(me)
                    return me;

                return me = Entity.FindIn(this);
            }
        }
    }
}
