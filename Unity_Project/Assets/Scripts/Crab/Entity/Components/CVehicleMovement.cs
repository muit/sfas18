﻿#if UNITY_EDITOR
using UnityEditor;
using CrabEditor;
#endif

namespace Crab.Entities
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.AI;

    [System.Serializable]
    public class AxleInfo : System.Object
    {
        public WheelCollider leftWheel;
        public WheelCollider rightWheel;
        public bool motor = false;
        public bool brake = true;
        public bool steering = false;

        [Range(-1, 1)]
        public float steeringCof = 1;
        public bool steerVisually = true;
    }

    [RequireComponent(typeof(Entity))]
    [DisallowMultipleComponent]
    public class CVehicleMovement : CBaseMovement
    {
        [Header("Handling")]
        public List<AxleInfo> axleInfos;
        public float maxMotorTorque = 700;
        public int maxVelocity = 10;
        public float maxSteeringAngle = 25;
        public float steeringSpeed = 5;
        public float steeringStoppedCof = 1;
        public float steeringMovingCof = 1;
        public float brakeForce = 300;
        public Vector3 centerOfMassCorrection;

        [Header("AI Movement")]
        public Transform targetTransform;
        public float reachDistance = 1.5f;

        bool isAIMoving = false;
        Vector3 targetPosition;
        int currentPathPoint = 3;
        NavMeshPath path;
        float currentStreeringValue;

        new Rigidbody rigidbody;


        protected override void Awake()
        {
            path = new NavMeshPath();
            rigidbody = GetComponent<Rigidbody>();

            if(targetTransform)
                AIMove(targetTransform);
        }

        public override void Move(float h, float v)
        {
            if (Mathf.Abs(h) > 0.01f || Mathf.Abs(v) > 0.01f)
            {
                moveVector = new Vector3(h * maxSteeringAngle, 0, v * maxMotorTorque);
            }
            else
            {
                moveVector = Vector3.zero;
            }
        }
        
        public override void AIMove(Vector3 position) {
            isAIMoving = true;
            targetPosition = position;
            targetTransform = null;
            UpdateAIPath();
        }

        public override void AIMove(Transform target)
        {
            if (target)
            {
                isAIMoving = true;
                targetTransform = target;
            }
        }

        public virtual void StopAIMove()
        {
            isAIMoving = false;
            currentPathPoint = 1;
        }

        protected override void AIUpdate()
        {
            if (isAIMoving)
            {
                if (currentPathPoint >= path.corners.Length)
                    StopAIMove();

                UpdateAIPath(true);

                if (path.status != NavMeshPathStatus.PathInvalid)
                {
                    for (int i = 0; i < path.corners.Length; i++)
                    {
                        if (i + 1 < path.corners.Length)
                        {
                            Debug.DrawLine(path.corners[i], path.corners[i + 1], Color.cyan);
                        }
                    }

                    if (path.corners.Length > currentPathPoint)
                    {
                        Vector3 toPathPoint = path.corners[currentPathPoint] - transform.position;
                        Debug.DrawLine(transform.position, transform.position + toPathPoint, Color.red);

                        if (toPathPoint.sqrMagnitude <= reachDistance * reachDistance)
                        {
                            currentPathPoint++;
                        }
                        else
                        {
                            Vector3 localPathDirection = transform.InverseTransformDirection(toPathPoint).normalized;
                            Move(localPathDirection.x, 1);
                        }
                    }
                    else
                    {
                        Move(0, 0);
                    }
                }
            }
        }
        

        protected override void MoveUpdate()
        {
            //Smooth steering
            currentStreeringValue = Mathf.Lerp(currentStreeringValue, moveVector.x, Time.deltaTime * steeringSpeed);


            bool shouldBrake = braking;

            /*float forwardVelocity = transform.InverseTransformDirection(rigidbody.velocity).x;


            Debug.Log(forwardVelocity);
            if ((moveVector.z >= 0 && forwardVelocity < 0) || (moveVector.z <= 0 && forwardVelocity > 0) ||
                (moveVector.z >= 0 && forwardVelocity < 0) || (moveVector.z <= 0 && forwardVelocity > 0))
            {
                shouldBrake = true;
                Debug.Log("Braking by 0 movement");
            }*/

            //0 wheel is stopped, 1 wheel is at maximum rate
            float velocity = rigidbody.velocity.magnitude;
            float speedCof = velocity / maxVelocity;

            bool speedExceeded = velocity > maxVelocity;

            foreach (AxleInfo axleInfo in axleInfos)
            {
                if (axleInfo.steering || axleInfo.motor)
                {
                    if (axleInfo.steering)
                    {
                        float steeringCof = Mathf.Lerp(steeringStoppedCof, steeringMovingCof, speedCof);
    
                        axleInfo.leftWheel.steerAngle = currentStreeringValue * axleInfo.steeringCof * steeringCof;
                        axleInfo.rightWheel.steerAngle = currentStreeringValue * axleInfo.steeringCof * steeringCof;
                    }

                    if (axleInfo.motor)
                    {
                        axleInfo.leftWheel.motorTorque  = speedCof >= 1? 0 : moveVector.z;
                        axleInfo.rightWheel.motorTorque = speedCof >= 1? 0 : moveVector.z;
                    }
                }

                //Braking
                if (axleInfo.brake && shouldBrake)
                {
                    axleInfo.leftWheel.brakeTorque = brakeForce;
                    axleInfo.rightWheel.brakeTorque = brakeForce;
                }
                else
                {
                    axleInfo.leftWheel.brakeTorque = 0;
                    axleInfo.rightWheel.brakeTorque = 0;
                }
                
                ApplyLocalPositionToVisuals(axleInfo.leftWheel, axleInfo.steerVisually);
                ApplyLocalPositionToVisuals(axleInfo.rightWheel, axleInfo.steerVisually);
            }
        }

        // Finds the corresponding visual wheel and correctly applies the transform
        public void ApplyLocalPositionToVisuals(WheelCollider collider, bool steerVisually)
        {
            if (collider.transform.childCount == 0)
            {
                return;
            }

            Transform visualWheel = collider.transform.GetChild(0);

            Vector3 position;
            Quaternion rotation;
            collider.GetWorldPose(out position, out rotation);

            visualWheel.transform.position = position;
            if(steerVisually)
                visualWheel.transform.rotation = rotation;
        }

        /**
        * Brakes
        */
        private bool braking;

        public void StartBraking()
        {
            if (!braking)
            {
                if(animator)
                    animator.SetTrigger("StartBraking");
            }
            braking = true;
            if (animator)
                animator.SetBool("Braking", braking);
        }

        public void StopBraking()
        {
            if (braking)
            {
                if (animator)
                    animator.SetTrigger("StopBraking");
            }
            braking = false;
            if (animator)
                animator.SetBool("Braking", braking);
        }

        public bool Braking { get { return braking; } }


        void UpdateAIPath(bool onlyForTransforms = false)
        {
            if (targetTransform)
            {
                if (NavMesh.CalculatePath(transform.position, targetTransform.position, NavMesh.AllAreas, path))
                    currentPathPoint = 1;
            }
            else if (!onlyForTransforms)
            {
                if (NavMesh.CalculatePath(transform.position, targetPosition, NavMesh.AllAreas, path))
                    currentPathPoint = 1;
            }
        }
    }


#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(AxleInfo))]
    class AxleInfoDrawer : PropertyDrawer
    {

        // Draw the property inside the given rect
        public override void OnGUI(Rect pos, SerializedProperty prop, GUIContent label)
        {
            EditorGUI.BeginProperty(pos, label, prop);

            var labelStyle = GUI.skin.GetStyle("Label");

            SerializedProperty leftProp = prop.FindPropertyRelative("leftWheel");
            SerializedProperty rightProp = prop.FindPropertyRelative("rightWheel");
            SerializedProperty motorProp = prop.FindPropertyRelative("motor");
            SerializedProperty brakeProp = prop.FindPropertyRelative("brake");
            SerializedProperty steeringProp = prop.FindPropertyRelative("steering");
            SerializedProperty steeringCofProp = prop.FindPropertyRelative("steeringCof");
            SerializedProperty steerVisuallyProp = prop.FindPropertyRelative("steerVisually");
            

            // Draw label
            //pos = EditorGUI.PrefixLabel(pos, GUIUtility.GetControlID(FocusType.Passive), label);

            prop.isExpanded = EditorGUI.Foldout(new Rect(pos.x, pos.y, pos.width, 15), prop.isExpanded, label);
            if (prop.isExpanded)
            {
                ++EditorGUI.indentLevel;

                // Calculate rects
                Rect leftLabelRect  = new Rect(pos.x,                 pos.y + 20, pos.width / 2,      16);
                Rect rightLabelRect = new Rect(pos.x + pos.width / 2, pos.y + 20, pos.width / 2,      16);
                Rect leftRect       = new Rect(pos.x,                 pos.y + 36, pos.width / 2, 16);
                Rect rightRect      = new Rect(pos.x + pos.width / 2, pos.y + 36, pos.width / 2, 16);


                EditorGUI.LabelField(leftLabelRect,  leftProp.displayName,  labelStyle);
                EditorGUI.LabelField(rightLabelRect, rightProp.displayName, labelStyle);
                
                EditorGUI.PropertyField(leftRect,  leftProp, GUIContent.none);
                EditorGUI.PropertyField(rightRect, rightProp, GUIContent.none);
                

                Rect motorRect    = new Rect(pos.x, pos.y + 60,  pos.width, 18);
                Rect brakeRect    = new Rect(pos.x, pos.y + 78,  pos.width, 18);
                Rect steeringRect = new Rect(pos.x, pos.y + 96, pos.width, 18);

                EditorGUI.PropertyField(motorRect,    motorProp);
                EditorGUI.PropertyField(brakeRect,    brakeProp);
                EditorGUI.PropertyField(steeringRect, steeringProp);

                if (steeringProp.boolValue)
                {
                    Rect steeringCofRect = new Rect(pos.x, pos.y + 114, pos.width, 16);
                    EditorGUI.PropertyField(steeringCofRect, steeringCofProp);
                    Rect steerVisuallyRect = new Rect(pos.x, pos.y + 132, pos.width, 16);
                    EditorGUI.PropertyField(steerVisuallyRect, steerVisuallyProp);
                }
                

                // Set indent back to what it was
                --EditorGUI.indentLevel;
            }
            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty prop, GUIContent label)
        {
            SerializedProperty steeringProp = prop.FindPropertyRelative("steering");

            float height = base.GetPropertyHeight(prop, label);

            if (prop.isExpanded)
            {
                height += 100;

                if (steeringProp.boolValue)
                    height += 38;
            }

            return height;
        }
    }
#endif
}
