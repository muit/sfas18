﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
using CrabEditor;
#endif

namespace Crab {

    using Entities;

    [System.Serializable]
    public class DamageReachedEvent : UnityEvent { }

    [System.Serializable]
    public class DamageReachedInfo
    {
        public bool checkPercentage = false;

        public int   minimumDamagePoints = 10;
        public float minimumPercentage = 30;
        public DamageReachedEvent onDamageReached;
    }

    [Serializable]
    public class SkinRenderer
    {
        public Renderer renderer;
        public Color tint = Color.white;
    }


    public class Entity : MonoBehaviour
    {
        public bool  controlledByFaction = false;
        public bool  despawnOnDead = true;
        public float destroyDelay = 0.2f;

        public DamageEvent OnAnyDamage;
        public DeadEvent   OnDead;

        public List<DamageReachedInfo> damageEvents = new List<DamageReachedInfo>();

        public EntityController controllerPrefab;

        [Header("Firing")]
        [SerializeField]
        private CTurret turret;
        public CWeapon weapon;

        [Header("Visuals")]
        public List<SkinRenderer> skinRenderers = new List<SkinRenderer>();

        public UnityAction OnDestroyEvent;

        [Header("Points")]
        public int deadPoints = 30;
        public int damagePoints = 5;


        public CTurret Turret { get { return turret; } }

        void Awake()
        {
            m_controller = GetComponent<EntityController>();
            turret       = GetComponent<CTurret>();

            //Generate Controller
            if (controlledByFaction)
            {
                m_controller = FactionDatabase.AddController(this, Attributes.faction);
            }
        }


        protected virtual void Start()
        {
            if (!controller)
            {
                if (controller = GetComponent<AIController>())
                {
                    controller.SetControlledEntity(this);
                }
            }
        }

        protected virtual void OnDestroy()
        {
            if(OnDestroyEvent != null)
                OnDestroyEvent();
        }

        //Public Methods
        public void Damage(int damage, Entity damageCauser, DamageType damageType = DamageType.DEFAULT) {
            if (!IsAlive() || damage < 1)
                return;

            Attributes.Live -= damage;

            foreach(DamageReachedInfo ev in damageEvents)
            {
                if(!ev.checkPercentage)
                {
                    if(Attributes.Live <= ev.minimumDamagePoints)
                    {
                        ev.onDamageReached.Invoke();
                    }
                }
                else
                {
                    if(Attributes.LivePercentage <= ev.minimumPercentage)
                    {
                        ev.onDamageReached.Invoke();
                    }
                }
            }

            AnyDamage(damage, damageCauser, damageType);

            if (!IsAlive())
            {
                JustDied(damageCauser ? damageCauser : this);
            }
            else
            {
                if (IsAI() && !ai.IsInCombatWith(damageCauser))
                {
                    ai.StartCombatWith(damageCauser);
                }

                if (damageCauser)
                {
                    damageCauser.AddPoints(damagePoints, transform.position, transform.rotation);
                }
            }
        }

        public void Die(Entity killer = null) {
            if (!IsAlive())
                return;

            Attributes.Live = 0;

            JustDied(killer);
        }

        public void Despawn(float delay = -1) {
            if (IsAlive())
                Die();

            if (delay < 0)
            {
                if (controller)
                    Destroy(controller.gameObject);

                Destroy(gameObject);
            }
            else
            {
                if (controller)
                    Destroy(controller.gameObject, delay);

                Destroy(gameObject, delay);
            }
        }
        
        public virtual void AnyDamage(int damage, Entity damageCauser, DamageType damageType)
        {
            OnAnyDamage.Invoke(damage, damageCauser);

            if (controller)
                controller.AnyDamage(damage, damageCauser, damageType);
        }

        public virtual void JustDied(Entity killer)
        {
            OnDead.Invoke(killer);

            if (controller)
            {
                controller.JustDied(killer);
            }

            if (killer && killer.controller)
            {
                killer.controller.SendMessage("JustKilled", this);
            }

            Destroyable[] destroyables = GetComponentsInChildren<Destroyable>();
            foreach (Destroyable destroyable in destroyables)
            {
                destroyable.ApplyDestruction(9999);
            }

            if (killer)
            {
                killer.AddPoints(deadPoints, transform.position, transform.rotation);
            }

            if (despawnOnDead)
            {
                Despawn(destroyDelay);
            }
        }

        public void AddPoints(int points, Vector3 location, Quaternion initialRotation)
        {
            if (IsPlayer() && points > 0)
            {
                PuntuationMarker marker = Instantiate(Cache.Get.puntuationMarkerPrefab, location, initialRotation);
                marker.SetTarget(Cache.Get.camera.transform);
                marker.SetPoints(points);
                ((SFASPlayerController)player_controller).AddPoints(points);
            }
        }

        public bool IsPlayer() {
            return m_controller as PlayerController;
        }
        
        //[SerializeField]
        private EntityController m_controller;

        //Never assign. Use controller.StopControlling()
        public EntityController controller {
            get { return m_controller; }
            set { m_controller = value; }
        }
        public PlayerController player_controller
        {
            get { return m_controller as PlayerController; }
        }


        private AIController m_ai;
        public AIController ai {
            get { return m_ai ? m_ai : m_ai = m_controller as AIController; }
        }
        public bool IsAI() {
            return ai;
        }



        //Components

        private CBaseMovement movement;
        public CBaseMovement Movement {
            get {
                return movement ? movement : movement = GetComponent<CBaseMovement>();
            }
        }

        private CState state;
        public CState State {
            get {
                return state ? state : state = GetComponent<CState>();
            }
        }

        private CAttributes attributes;
        public CAttributes Attributes {
            get {
                return attributes ? attributes : attributes = GetComponent<CAttributes>();
            }
        }

        private CInventory inventory;
        public CInventory Inventory {
            get {
                return inventory? inventory : inventory = GetComponent<CInventory>();
            }
        }

        public bool IsEnemyOf(Entity entity) {
            if (!entity) return false;
            if (this == entity) return false;
            if (!Attributes || !entity.Attributes) return true;
            if (Attributes.faction != entity.Attributes.faction) return true;
            if (Attributes.faction == FactionDatabase.NO_FACTION && 
                entity.Attributes.faction == FactionDatabase.NO_FACTION) return true;
            return false;
        }

        public bool IsAlive() {
            return Attributes? Attributes.IsAlive() : false;
        }


        public float DistanceTo(Entity other) {
            return other? Vector3.Distance(transform.position, other.transform.position) : 65536.0f;
        }


        /**
         * STATIC
         */

        public static Entity FindIn(Component obj) {
            if (!obj)
                return null;

            Entity entity = obj.GetComponent<Entity>();

            if (!entity)
                entity = obj.GetComponentInParent<Entity>();

            return entity;
        }

        public static Entity FindIn(GameObject obj)
        {
            if (!obj)
                return null;

            Entity entity = obj.GetComponent<Entity>();

            if (!entity)
                entity = obj.GetComponentInParent<Entity>();

            return entity;
        }

        public void UpdateSkin(Color color)
        {
            foreach (var skin in skinRenderers)
            {
                if (skin.renderer)
                {
                    skin.renderer.material.SetColor(Shader.PropertyToID("_Color"), color * skin.tint);
                }
            }
        }
    }
}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(Crab.SkinRenderer))]
class SkinRendererDrawer : PropertyDrawer
{
    // Draw the property inside the given rect
    public override void OnGUI(Rect pos, SerializedProperty prop, GUIContent label)
    {
        SerializedProperty rendererProp = prop.FindPropertyRelative("renderer");
        SerializedProperty tintProp = prop.FindPropertyRelative("tint");

        // Draw label
        pos = EditorGUI.PrefixLabel(pos, GUIUtility.GetControlID(FocusType.Passive), new GUIContent(rendererProp.displayName));

        Rect rendererRect = new Rect(pos.x, pos.y, pos.width * 0.8f, 18);
        Rect tintRect = new Rect(pos.x - 10 + pos.width * 0.8f, pos.y, pos.width * 0.2f + 10, 18);

        EditorGUI.PropertyField(rendererRect, rendererProp, GUIContent.none);
        EditorGUI.PropertyField(tintRect, tintProp, GUIContent.none);
    }
}
#endif
