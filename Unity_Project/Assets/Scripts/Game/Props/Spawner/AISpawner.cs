﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crab;
using Crab.Utils;

[System.Serializable]
public class SpawnPreset {
    [Tooltip("Entity that will be generated")]
    public Entity entityPrefab;
    public int spawnsPerMinute = 21;
    public int spawnVariation = 3;
    [Tooltip("Maximum amount of spawns")]
    public int maxSpawns = 100;
}

public class AISpawner : MonoBehaviour
{

    public bool startActivated = false;
    
    [Tooltip("(Optional) Where the new entity will be added")]
    public GameObject containerObject;

    [Header("Entity")]
    public bool startCombatOnSpawn = true;

    public SpawnPreset defaultPreset;
    public List<SpawnPreset> roundPresets;

    private Delay spawnDelay = new Delay(1, false);

    // Use this for initialization
    void Start () {
        spawnDelay = new Delay(GetSpawnLength(), startActivated);
	}

    public void Activate() {
        spawnDelay.Start(GetSpawnLength());
    }
	
	// Update is called once per frame
	void Update () {
        if (!spawnDelay.Over())
            return;

        //Start the event again
        spawnDelay.Start(GetSpawnLength());
        
        SpawnPreset preset = GetPreset();

        if (!preset.entityPrefab)
            return;

        Entity newEntity = SceneScript.SpawnEntity(transform, preset.entityPrefab, transform.position, transform.rotation);

        if (newEntity)
        {
            if (containerObject)
            {
                newEntity.transform.parent = containerObject.transform;
            }

            if (startCombatOnSpawn && newEntity.ai)
            {
                //Start combat with first player
                newEntity.ai.StartCombatWith(SceneScript.Get(this).GetPlayer(0).Me);
            }
        }
	}

    private float GetSpawnLength() {
        SpawnPreset preset = GetPreset();

        float finalSpawnsPerMinute = Mathf.Clamp(Random.Range(preset.spawnsPerMinute - preset.spawnVariation, preset.spawnsPerMinute + preset.spawnVariation), 1, 100000);
        return 60f/finalSpawnsPerMinute;
    }

    private SpawnPreset GetPreset()
    {
        SpawnPreset preset = null;
        
        ArcadeGameScene scene = SceneScript.Get<ArcadeGameScene>(this);
        if (scene != null && scene.Round >= 0 && roundPresets.Count > scene.Round)
            preset = roundPresets[scene.Round];
        
        if (preset == null)
            return defaultPreset;

        return preset;
    }
}
