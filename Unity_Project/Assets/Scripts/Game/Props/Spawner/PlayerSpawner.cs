﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crab;
using Crab.Utils;


public class PlayerSpawner : MonoBehaviour
{

    public bool startActivated = false;
    
    public Entity m_playerPrefab;

    [Tooltip("(Optional) Where the new entity will be added")]
    public GameObject containerObject;

    private void Start()
    {
        if (startActivated)
            Activate();
    }

    public void Activate(Entity playerPrefab = null)
    {
        if (!playerPrefab)
            playerPrefab = m_playerPrefab;

        if (!playerPrefab)
            return;

        Entity newEntity = SceneScript.SpawnEntity(transform, playerPrefab, transform.position, transform.rotation);

        if (containerObject)
        {
            newEntity.transform.parent = containerObject.transform;
        }
    }
}
