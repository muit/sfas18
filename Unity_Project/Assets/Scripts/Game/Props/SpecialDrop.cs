﻿using Crab;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialDrop : MonoBehaviour {
    
    public string vehicle;
    public float duration;


    public void PickUp() {
        ArcadeGameScene scene = SceneScript.Get<ArcadeGameScene>(this);

        scene.ActivateSpecialVehicle(vehicle, duration);
    }
}
