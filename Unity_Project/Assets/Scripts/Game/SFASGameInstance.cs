﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crab;
using UnityEngine.SceneManagement;

public enum ControlMode {
    Keyboard,
    Gamepad
}

public enum GameMode
{
    None,
    Local,
    Arcade
}

public class SFASGameInstance : GameInstance {
    // Static singleton property
    public static SFASGameInstance GetSFAS {
        get { return (SFASGameInstance)Get; }
    }

    public ControlMode controlMode = ControlMode.Keyboard;
    public string menuScene = "Menu";
    public GameMode gameMode = GameMode.None;
    public string localGameScene  = "MatchLocal";
    public string arcadeGameScene = "MatchArcade";

    Scene menu;
    Scene game;


    protected override void Construct()
    {
        GameMode currentGameMode = GetCurrentSceneGameMode();

        // If we are in a game scene, start match automatically
        if(currentGameMode != GameMode.None)
        {
            StartGame(currentGameMode);
        }
        // Other way, try to load Menu
        else
        {
            GoMenu();
        }
    }

    public void GoMenu() {
        menu = LoadScene(menuScene);
    }

    public void StartGame(GameMode gameMode = GameMode.None, bool hardReset = false)
    {
        if (gameMode == GameMode.None)
            return;

        this.gameMode = gameMode;

        LoadScene(GetGameModeScene(), LoadSceneMode.Single, !hardReset, (Scene scene) => {
            game = scene;

            //Wait for level to be loaded

            //Get a game-object from game scene to ensure we find the correct level script
            GameObject contextGO = scene.GetRootGameObjects()[0];
            
            SceneScript.Get<GameScene>(contextGO).StartMatch();
        });
    }

    GameMode GetCurrentSceneGameMode() {
        Scene currentScene = SceneManager.GetActiveScene();
        if (currentScene.name == arcadeGameScene)
            return GameMode.Arcade;
        else if (currentScene.name == localGameScene)
            return GameMode.Local;

        return GameMode.None;
    }

    public void OnStartRound()
    {
    }
    public void OnFinishRound()
    {
    }

    public void FinishMatch(GameFinishReason reason) {
    }

    public string GetGameModeScene()
    {
        return gameMode == GameMode.Arcade ? arcadeGameScene : localGameScene; 
    }
}
