﻿using Crab;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetMarker : MonoBehaviour
{
    [SerializeField]
    protected bool selected = false;
    public Image image;
    public LineRenderer line;


    Entity bindedEntity;
    PlayerController primaryOwner = null;
    PlayerController[] markerOwners = new PlayerController[2];


    void Start()
    {
        LookAtTarget[] lookAtTargets = GetComponentsInChildren<LookAtTarget>();

        foreach (LookAtTarget lookAtTarget in lookAtTargets)
        {
            lookAtTarget.target = Cache.Get.camera.transform;
        }
        bindedEntity = GetComponentInParent<Entity>();
    }

    private void Update()
    {
        if (line && primaryOwner && primaryOwner.Me.Turret)
        {
            line.enabled = selected;
            if (selected)
            {
                line.SetPosition(1, primaryOwner.Me.Turret.objects[0].transform.position);
                line.SetPosition(0, primaryOwner.Me.Turret.TargetPosition);
            }
        }

        if (bindedEntity && !bindedEntity.IsAlive())
        {
            // Transparent
            SetColor(new Color(0, 0, 0, 0));
        }
    }

    public bool Mark(PlayerController owner) {
        if (!owner)
            return false;

        bool applied = false;
        if (!markerOwners[0])
        {
            markerOwners[0] = owner;
            applied = true;
        }
        else if (!markerOwners[1])
        {
            markerOwners[1] = owner;
            applied = true;
        }

        if (applied)
        {
            primaryOwner = owner;
            UpdateStyle();
        }

        return applied;
    }

    public void UnMark(PlayerController owner) {
        bool canContinue = false;

        if (primaryOwner == owner) {
            primaryOwner = null;
        }

        if (markerOwners[0] == owner) {
            markerOwners[0] = null;
            canContinue = true;
        } else if (markerOwners[1] == owner) {
            markerOwners[1] = null;
            canContinue = true;
        }

        if (canContinue && !markerOwners[1] && !markerOwners[1])
        {
            Destroy(gameObject);
        }
    }

    public void Select(PlayerController owner, bool value)
    {
        if (!owner)
            return;

        primaryOwner = owner;
        selected = value;
        
        UpdateStyle();
    }

    public void UpdateStyle()
    {
        if (bindedEntity && !bindedEntity.IsAlive())
        {
            // Transparent
            SetColor(new Color(0, 0, 0, 0));
            return;
        }

        if (primaryOwner)
        {
            PlayerInfo info = SceneScript.Get<GameScene>(this).GetPlayerInfo(primaryOwner.PlayerId);
            if (info != null)
            {
                SetColor(info.color);
                return;
            }
        }

        // Transparent
        SetColor(new Color(0, 0, 0, 0));
    }

    public void SetColor(Color color)
    {
        if (!image)
            return;

        if (selected)
        {
            color += Color.HSVToRGB(0, 0.6f, 0);
            image.rectTransform.localScale = new Vector3(1.5f, 1.5f, 1);
        }
        else
        {
            image.rectTransform.localScale = new Vector3(1, 1, 1);
        }

        image.color = color;
    }
}
