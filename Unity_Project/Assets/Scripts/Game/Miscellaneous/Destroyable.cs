﻿using Crab;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyable : MonoBehaviour {

    public int health = 1;

    private Entity owner;

    private void Start()
    {
        owner = Entity.FindIn(this);

        if (owner)
            owner.OnDestroyEvent += OnOwnerDestroyed;
    }

    private void OnDestroy()
    {
        if (owner)
            owner.OnDestroyEvent -= OnOwnerDestroyed;
    }

    public void ApplyDestruction(int damage)
    {
        if (IsDestroyed())
            return;

        health -= damage;
        if (health <= 0)
        {
            //Add physics
            gameObject.AddComponent<Rigidbody>();

            //Set root parent
            transform.parent = null;
        }
    }

    public bool IsDestroyed()
    {
        return health <= 0;
    }
    
    protected virtual void OnOwnerDestroyed()
    {
        Destroy(gameObject);
    }
}
