﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelExtender : MonoBehaviour {

    new Rigidbody rigidbody;
    WheelCollider wheel;

	// Use this for initialization
	void Start () {
        rigidbody = GetComponentInParent<Rigidbody>();
        wheel = GetComponent<WheelCollider>();
	}

    public int antiRollStrength = 1000;//How intense the anti roll force is.
    public int minimumVelocity = 4;
    public int maximumVelocity = 10;

    void Update()
    {
        AntiSway();
    }

    void AntiSway()
    {
        WheelHit hit;

        //If a tire leaves the ground
        if (!wheel.GetGroundHit(out hit))
        {
            
            // Dont apply if we are not moving
            float strenghCof = Mathf.Clamp01((rigidbody.velocity.magnitude - minimumVelocity) / maximumVelocity);

            //Apply a downward force at the location of this wheel
            rigidbody.AddRelativeForce(Vector3.down * antiRollStrength * strenghCof, ForceMode.Impulse);
            
            //Apply constant torque force towards relative down. Helps recovering from overturns
            float rotateCof = Vector3.SignedAngle(-transform.up, Vector3.down, transform.forward) / 180;
            rigidbody.AddRelativeTorque(Vector3.forward * 200 * rotateCof, ForceMode.Impulse);
        }
    }
}
