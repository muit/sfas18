﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PuntuationMarker : MonoBehaviour {

    public Text text;
    public List<LookAtTarget> lookAtTargets = new List<LookAtTarget>();

    public void SetPoints(int points) {
        text.text = "+" + points;
    }

    public void SetTarget(Transform target)
    {
        foreach (LookAtTarget lookAtTarget in lookAtTargets)
        {
            if (lookAtTarget)
            {
                lookAtTarget.target = target;
            }
        }
    }
}
