﻿using UnityEngine;
using Crab.Entities;
using System.Collections.Generic;
using Crab;


public enum Axis {
    X,
    Y,
    Z
}


[System.Serializable]
public class TurretRotable {
    public Transform transform;
    public Axis axis = Axis.Y;
    public float minSpeed = 0;
    public float speed = 3;
    public Vector3 forwardRotation;
    public float minAngle = 0;
    public float maxAngle = 0;
}


public class CTurret : EntityComponent
{
    public float maxFireAngle = 45;
    
    public List<TurretRotable> objects = new List<TurretRotable>();

    [SerializeField]
    Vector3 targetDirection = Vector3.zero;
    [SerializeField]
    Transform targetTransform;
    Vector3 targetPosition = Vector3.zero;

    bool wantsToFire = false;

    public Vector3 TargetPosition {
        get { return targetTransform ? targetTransform.position : targetPosition; }
    }


    public void SetLookDirection(Vector3 direction) {
        targetDirection = direction;
        targetTransform = null;
        targetPosition = Vector3.zero;
    }

    public void SetLookTarget(Transform target)
    {
        targetTransform = target;
        targetPosition = Vector3.zero;
    }

    public void SetLookTarget(Vector3 target)
    {
        targetTransform = null;
        targetPosition = target;
    }


    private void Start()
    {
        if (targetDirection == Vector3.zero)
        {
            targetDirection = transform.forward;
        }
    }


    private void Update()
    {
        bool readyForFiring = true;
        bool lookToTransform = targetTransform;
        bool lookToPosition = targetPosition != Vector3.zero;

        foreach (TurretRotable turretObj in objects)
        {
            Transform rotable = turretObj.transform;

            if (rotable)
            {
                //Calculate direction if needed by transform or position
                if (lookToTransform)
                {
                    targetDirection = targetTransform.position - turretObj.transform.position;
                }
                else if (lookToPosition)
                {
                    targetDirection = targetPosition - turretObj.transform.position;
                }

                Debug.DrawLine(rotable.position, rotable.position + targetDirection, Color.green);


                //Apply global rotation
                Quaternion targetRotation = Quaternion.LookRotation(targetDirection);
                {
                    rotable.rotation = Quaternion.RotateTowards(rotable.rotation, targetRotation, turretObj.speed * Time.deltaTime);

                    Debug.DrawLine(rotable.position, rotable.position + targetRotation * Vector3.left, Color.red);
                }


                //Keep only the selected axis's local rotation
                switch (turretObj.axis)
                {
                    case Axis.X:
                        rotable.localRotation = Quaternion.Euler(ClampAngle(turretObj, rotable.localRotation.eulerAngles.x), 0, 0);
                        break;
                    case Axis.Y:
                        rotable.localRotation = Quaternion.Euler(0, ClampAngle(turretObj, rotable.localRotation.eulerAngles.y), 0);
                        break;
                    case Axis.Z:
                        rotable.localRotation = Quaternion.Euler(0, 0, ClampAngle(turretObj, rotable.localRotation.eulerAngles.z));
                        break;
                }


                readyForFiring &= Vector3.Angle(rotable.forward, targetDirection) < maxFireAngle;
            }
        }

        if (Me.weapon)
        {
            if (wantsToFire && readyForFiring)
            {
                Me.weapon.Fire(Entity.FindIn(targetTransform));
            }
            else
            {
                Me.weapon.FinishFiring();
            }
        }
    }

    public void Fire()
    {
        wantsToFire = true;
    }

    public void FinishFiring()
    {
        wantsToFire = false;
    }

    private float ClampAngle(TurretRotable rotable, float angle)
    {
        if (rotable.minAngle == 0 && rotable.maxAngle == 0)
            return angle;

        if (angle > 180)
            angle = 360 - angle;

        angle = Mathf.Clamp(angle, rotable.minAngle, rotable.maxAngle);

        if (angle < 0)
            angle = 360 + angle;

        return angle;
    }
}
