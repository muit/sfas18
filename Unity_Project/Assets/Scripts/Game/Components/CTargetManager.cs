﻿
using UnityEngine;
using Crab;
using Crab.Entities;
using System.Collections.Generic;
using System;

public class TargetInfo
{
    public TargetInfo(Entity entity, TargetMarker marker = null) {
        m_entity = entity;
        m_marker = marker;
    }

    public Entity m_entity;
    public TargetMarker m_marker;

    public bool Equals(TargetInfo other)
    {
        return m_entity == other.m_entity;
    }
}

[RequireComponent(typeof(Entity))]
[DisallowMultipleComponent]
public class CTargetManager : EntityComponent
{

    public TargetMarker m_markerPrefab;

    List<TargetInfo> targets = new List<TargetInfo>();
    TargetInfo activeTarget;

    public void AddTarget(Entity newTarget)
    {
        if (newTarget && m_markerPrefab)
        {
            //Is it my self?
            if (newTarget == Me)
                return;

            TargetInfo info = new TargetInfo(newTarget);

            //Find or Create marker
            TargetMarker marker = newTarget.GetComponentInChildren<TargetMarker>();


            if (!marker)
                marker = TargetMarker.Instantiate(m_markerPrefab, newTarget.transform);

            marker.transform.localPosition = Vector3.zero;
            marker.Mark(Me.player_controller);
            info.m_marker = marker;


            targets.Add(info);
        }
    }

    public void RemoveTarget(Entity target)
    {
        int infoId = targets.FindIndex(e =>
        {
            return e.m_entity == target;
        });

        if (infoId >= 0)
        {
            TargetInfo info = targets[infoId];

            if (info.m_marker)
            {
                info.m_marker.UnMark(Me.player_controller);
            }
        }
    }

    public void SelectActiveTarget() {
        // Setup puntuation filters
        List<TargetTest> tests = new List<TargetTest>();
        tests.Add(new DistanceTargetTest(Me, 15,  0,       FilterType.FilterAndScore));
        tests.Add(new AngleTargetTest   (Me, 120, 0, true, FilterType.FilterAndScore));
        tests.Add(new BoolTargetTest    (Me, (TargetInfo target) => {
            return target.m_entity.IsAI();
        }, FilterType.ScoreOnly));


        TargetInfo winnerTarget = null;
        float winnerPuntuation = 0f;

        foreach(TargetInfo target in targets)
        {
            float puntuation = 0f;

            foreach (TargetTest test in tests)
            {
                if (test != null)
                {
                    bool success = false;
                    puntuation += test.Evaluate(target, ref success);

                    if (!success)
                    {
                        //Filtered away
                        puntuation = 0;
                        break;
                    }
                }
            }

            if (winnerTarget == null || puntuation > winnerPuntuation)
            {
                winnerTarget = target;
                winnerPuntuation = puntuation;
            }
        }

        if (winnerTarget != null && winnerPuntuation > 0)
        {
            SetActiveTarget(winnerTarget);
        }
    }

    public void SetActiveTarget(Entity entity)
    {
        int infoId = targets.FindIndex(e =>
        {
            return e.m_entity == entity;
        });

        if (infoId >= 0)
        {
            SetActiveTarget(targets[infoId]);
        }
    }

    public void SetActiveTarget(TargetInfo info)
    {
        if (info != null && info != activeTarget && targets.Contains(info))
        {
            //Clean last target marker
            if (activeTarget != null && activeTarget.m_marker)
            {
                activeTarget.m_marker.Select(Me.player_controller, false);
            }

            activeTarget = info;
            activeTarget.m_marker.Select(Me.player_controller, true);
        }
    }

    public TargetInfo GetActiveTarget() {
        return activeTarget;
    }
}

public enum FilterType { FilterAndScore, FilterOnly, ScoreOnly }

public class TargetTest
{
    public TargetTest(Entity entity, FilterType inMode = FilterType.ScoreOnly, float inMultiplier = 1)
    {
        mode = inMode;
        m_entity = entity;
        multiplier = inMultiplier;
    }

    public FilterType mode;
    public Entity m_entity;
    public float multiplier;

    public float Evaluate(TargetInfo target, ref bool success)
    {
        float result = Eval(target) * multiplier;

        switch(mode)
        {
            case FilterType.FilterAndScore:
                success = result > 0;
                break;
            case FilterType.FilterOnly:
                success = result > 0;
                return 0;
            case FilterType.ScoreOnly:
                success = true;
                break;
        }

        return result;
    }

    protected virtual float Eval(TargetInfo target)
    {
        return 1.0f;
    }
}

// 0 puntuation at minDistance, 1 at maxDistance
public class DistanceTargetTest : TargetTest
{
    public DistanceTargetTest(Entity entity, float inMinDistance, float inMaxDistance, FilterType inMode = FilterType.ScoreOnly, float inMultiplier = 1) : base(entity, inMode, inMultiplier)
    {
        minDistance = inMinDistance;
        maxDistance = inMaxDistance;
    }

    public float minDistance = 0;
    public float maxDistance = 10;


    protected override float Eval(TargetInfo target)
    {
        if (!target.m_entity)
            return 0f;

        float distance = (m_entity.transform.position - target.m_entity.transform.position).magnitude;

        return Mathf.Clamp01(Mathf.InverseLerp(minDistance, maxDistance, distance));
    }
}


// 0 puntuation at minAngle, 1 at maxAngle
public class AngleTargetTest : TargetTest
{
    public AngleTargetTest(Entity entity, float inMinAngle, float inMaxAngle, bool inIgnoreHeight = true, FilterType inMode = FilterType.ScoreOnly, float inMultiplier = 1) : base(entity, inMode, inMultiplier)
    {
        minAngle = inMinAngle;
        maxAngle = inMaxAngle;
        ignoreHeight = inIgnoreHeight;
    }

    public float minAngle = 180;
    public float maxAngle = 0;
    public bool ignoreHeight = true;


    protected override float Eval(TargetInfo target)
    {
        if (!target.m_entity)
            return 0f;

        Vector3 direction = target.m_entity.transform.position - m_entity.transform.position;
        Vector3 forward  = m_entity.transform.forward;

        if (ignoreHeight)
        {
            direction.y = 0;
            forward.y = 0;
        }
        float angle = Vector3.Angle(forward, direction);
        
        return Mathf.Clamp01(Mathf.InverseLerp(minAngle, maxAngle, angle));
    }
}


// 1 when target type is the same, 0 when not
public class BoolTargetTest : TargetTest
{
    public BoolTargetTest(Entity entity, Func<TargetInfo, bool> inPredicate, FilterType inMode = FilterType.ScoreOnly, float inMultiplier = 1) : base(entity, inMode, inMultiplier)
    {
        predicate = inPredicate;
    }

    public Func<TargetInfo, bool> predicate;

    protected override float Eval(TargetInfo target)
    {
        if (!target.m_entity || predicate != null)
            return 0f;

        return predicate(target)? 1 : 0;
    }
}
