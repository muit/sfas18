﻿using System;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Crab;
using Crab.Utils;
using Crab.Entities;

[Serializable]
public class PlayerModule
{
    public Entity Me {
        get { return controller? controller.Me : null; }
    }

    public virtual void Initialize( SFASPlayerController con) {
        controller = con;
    }


    [NonSerialized]
    public SFASPlayerController controller;


    public virtual void OnPossess() {}

    public virtual void JustKilled(Entity victim) {}

    public virtual void MovementUpdate() { }
    public virtual void CombatUpdate() { }
}


public class SFASPlayerController : PlayerController {

    [Header("Targeting")]
    public LayerMask targetableMasks;
    public float targetDetectionRadius = 40;

    [Header("GameMode customs")]
    public ArcadePlayerModule arcadeMode;
    public LocalPlayerModule  localMode;


    protected override void OnPossess() {
        base.OnPossess();
        
        if (Cam)
        {
            Cam.AddTarget(Me.transform);
        }


        arcadeMode.Initialize(this);
        localMode.Initialize(this);

        if (SFASGameInstance.GetSFAS.gameMode == GameMode.Arcade)
        {
            arcadeMode.OnPossess();
        }
        else if (SFASGameInstance.GetSFAS.gameMode == GameMode.Local)
        {
            localMode.OnPossess();
        }

        
        //Paint skins with player colors
        PlayerInfo info = SceneScript.Get<GameScene>(this).GetPlayerInfo(PlayerId);
        if (info != null)
        {
            Me.UpdateSkin(info.color);
        }
    }

    protected override void JustDespawned()
    {
        if (!Cache.Get)
            return;

        BaseControlledCamera camera = Cache.Get.camera;
        if (camera)
            camera.RemoveTarget(transform);
    }

    protected override void JustKilled(Entity victim)
    {
        if (SFASGameInstance.GetSFAS.gameMode == GameMode.Arcade)
        {
            arcadeMode.JustKilled(victim);
        }
        else if (SFASGameInstance.GetSFAS.gameMode == GameMode.Local)
        {
            localMode.JustKilled(victim);
        }
    }


    protected override void Update()
    {
        if (!me)
            return;

        if (!me.IsAlive())
        {
            me.Movement.Move(0,0);
            return;
        }

        MovementUpdate();

        CombatUpdate();
    }

    protected virtual void MovementUpdate()
    {
        if (PlayerId > 1)
            return;

        if (SFASGameInstance.GetSFAS.gameMode == GameMode.Arcade)
        {
            arcadeMode.MovementUpdate();
        }
        else if (SFASGameInstance.GetSFAS.gameMode == GameMode.Local)
        {
            localMode.MovementUpdate();
        }
    }

    protected virtual void CombatUpdate()
    {
        if (SFASGameInstance.GetSFAS.gameMode == GameMode.Arcade)
        {
            arcadeMode.CombatUpdate();
        }
        else if (SFASGameInstance.GetSFAS.gameMode == GameMode.Local)
        {
            localMode.CombatUpdate();
        }
    }

    public void AddPoints(int points) {
        if (points > 0)
        {
            GameScene scene = SceneScript.Get<GameScene>(this);
            PlayerInfo info = scene.GetPlayerInfo(PlayerId);

            if (info != null)
            {
                info.score += points;
                scene.JustChangedScore();
            }
        }
    }


    ///////////////////////////////////////////////////////////////////////////
    // FIRING

    public void HandleFiring(Entity target = null) {
        if(Me.weapon)
            Me.weapon.Fire(target);
    }
}
