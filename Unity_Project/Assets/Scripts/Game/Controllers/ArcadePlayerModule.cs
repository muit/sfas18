﻿using Crab;
using Crab.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


[Serializable]
public class ArcadePlayerModule : PlayerModule {
    [Header("Aiming")]
    public LayerMask aimLayerMask;

    public override void OnPossess()
    {
        base.OnPossess();


    }

    public override void MovementUpdate()
    {
        ///////////////////////////////////////////////////////////////////////
        // MOVING

        float sideCof = Input.GetAxis("Horizontal_P" + (controller.PlayerId + 1));
        float forwardCof = Input.GetAxis("Vertical_P" + (controller.PlayerId + 1));
        Me.Movement.Move(sideCof, forwardCof);


        if (Me.Movement.IsVehicle())
        {
            CVehicleMovement vMovement = (CVehicleMovement)Me.Movement;
            if (Input.GetButtonDown("Brake_P" + (controller.PlayerId + 1)))
            {
                vMovement.StartBraking();
            }
            else if (Input.GetButtonUp("Brake_P" + (controller.PlayerId + 1)))
            {
                vMovement.StopBraking();
            }
        }
    }

    public override void CombatUpdate()
    {
        ///////////////////////////////////////////////////////////////////////
        // COMBAT

        //Find target
        
        BaseControlledCamera cam = Cache.Get.camera;
        if (Me.Turret)
        {
            RaycastHit[] hits = Physics.RaycastAll(cam.GetTargetPosition(), cam.transform.forward, 1000f, aimLayerMask);
            if (hits.Length > 0)
            {
                Me.Turret.SetLookTarget(hits[hits.Length - 1].point);
                Debug.DrawRay(cam.GetTargetPosition(), cam.transform.forward, Color.blue, 1);
            }
            else
            {
                Me.Turret.SetLookDirection(cam.transform.forward);
                Debug.DrawRay(cam.GetTargetPosition(), cam.transform.forward, Color.gray);
            }
        }


        if (Input.GetButton("Fire1"))
        {
            controller.HandleFiring();
        }
    }
}
