﻿using UnityEngine;
using System.Collections.Generic;
using Crab.Entities;

namespace Crab
{
    [DisallowMultipleComponent]
    public class AIController : EntityController
    {
        enum Events {
            BASIC_ATTACK = 20122
        }

        [Header("Range")]
        public bool doRange = true;
        public float rangeAttackDistance = 15.0f;

        
        //Public Methods
        protected List<EntityController> potentialTargets = new List<EntityController>();

        public EntityController Target {
            get { return IsInCombat() ? potentialTargets[0] : null; }
        }


        private CBaseMovement Movement {
            get { return Me.Movement; }
        }

        protected override void OnPossess()
        {
        }

        protected override void Update()
        {
            base.Update();

            //Remove invalid targets
            if (potentialTargets.Count > 0 && potentialTargets[0] == null)
                potentialTargets.RemoveAt(0);

            if (IsInCombat())
                CombatUpdate();
            else
            {
                //Reset turret if no target
                Me.Turret.SetLookDirection(transform.forward);
                Me.Turret.FinishFiring();
            }
        }

        protected virtual void CombatUpdate()
        {
            if (IsInCombat())
            {
                Me.Turret.SetLookTarget(Target.Me.transform);
                Me.Turret.Fire();
            }
        }

        protected override void EnterCombat(Entity target)
        {
            Movement.AIMove(target.transform);
        }

        protected override void FinishCombat(Entity enemy)
        {
            if (enemy)
            {
                //Move to last known target position
                Movement.AIMove(enemy.transform.position);
            }
        }
        
        public override void JustDied(Entity killer)
        {
            base.JustDied(killer);

            StopCombat();
        }

        protected override void JustKilled(Entity victim) {
            if (IsInCombatWith(victim))
            {
                StopCombatWith(victim);
            }
        }

        protected virtual void OnEvent(int id)
        {
            //switch ((Events)id)
            {
            }
        }

        public void StartCombatWith(Entity entity) {
            if (!Me.IsAlive())
                return;

            if (!entity || !entity.controller || !Me.IsEnemyOf(entity))
                return;

            potentialTargets.Add(entity.controller);
            EnterCombat(entity);
        }

        public void StopCombatWith(Entity entity)
        {
            if (IsInCombat() && entity && entity.controller)
            {
                potentialTargets.Remove(entity.controller);
                if (potentialTargets.Count == 0)
                {
                    FinishCombat(entity);
                }
            }
        }

        public void StopCombat()
        {
            if (IsInCombat())
            {
                potentialTargets.Clear();
                FinishCombat(null);
            }
        }

        public bool IsInCombat() {
            return potentialTargets.Count > 0 && potentialTargets[0];
        }

        public bool IsInCombatWith(Entity entity) {
            if (!entity || !entity.controller)
                return false;
            return potentialTargets.Contains(entity.controller);
        }

        public bool CanFire() {
            return Me.weapon && IsInCombat();
        }
    }
}