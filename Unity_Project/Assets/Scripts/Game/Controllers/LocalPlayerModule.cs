﻿using Crab;
using Crab.Entities;
using Crab.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class LocalPlayerModule : PlayerModule {

    public CTargetManager targetManager;

    Delay updateTargetTimer;

    public override void OnPossess()
    {
        base.OnPossess();

        updateTargetTimer = new Delay(0.2f);
        targetManager = Me.GetComponent<CTargetManager>();
    }

    public override void JustKilled(Entity victim)
    {
        //Find new target just after we kill
        if(targetManager)
            targetManager.SelectActiveTarget();
    }

    public override void MovementUpdate()
    {
        ///////////////////////////////////////////////////////////////////////
        // MOVING

        float sideCof = Input.GetAxis("Horizontal_P" + (controller.PlayerId + 1));
        float forwardCof = Input.GetAxis("Vertical_P" + (controller.PlayerId + 1));

        Me.Movement.Move(sideCof, forwardCof);


        if (Me.Movement.IsVehicle())
        {
            CVehicleMovement vMovement = (CVehicleMovement)Me.Movement;
            if (Input.GetButtonDown("Brake_P" + (controller.PlayerId + 1)))
            {
                vMovement.StartBraking();
            }
            else if (Input.GetButtonUp("Brake_P" + (controller.PlayerId + 1)))
            {
                vMovement.StopBraking();
            }
        }
    }

    public override void CombatUpdate()
    {
        if (!Me)
            return;

        ///////////////////////////////////////////////////////////////////////
        // COMBAT

        // Aim towards the best target
        if (targetManager) {

            if (updateTargetTimer.Over())
            {
                updateTargetTimer.Start();
                targetManager.SelectActiveTarget();
            }


            CTurret turret = Me.Turret;
            if (turret)
            {
                TargetInfo activeTarget = targetManager.GetActiveTarget();
                if (activeTarget != null && activeTarget.m_entity)
                {
                    turret.SetLookTarget(activeTarget.m_entity.transform);
                }
                else
                {
                    turret.SetLookDirection(Me.transform.forward);
                }
            }
        }

        if (Input.GetButton("Action_P" + (controller.PlayerId + 1)))
        {
            controller.HandleFiring();
        }
    }
}
