﻿using Crab;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuScene : SceneScript
{
    public void ClickedPlayArcade()
    {
        SFASGameInstance.GetSFAS.StartGame(GameMode.Arcade);
    }

    public void ClickedPlayLocal()
    {
        SFASGameInstance.GetSFAS.StartGame(GameMode.Local);
    }

    public void ClickedExit()
    {
        Application.Quit();
    }
}
