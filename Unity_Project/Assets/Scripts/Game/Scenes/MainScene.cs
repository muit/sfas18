﻿using Crab;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainScene : SceneScript
{
    public Camera cam;
    public Canvas loadingCanvas;

    protected override void Awake()
    {
        GameInstance.OnLoadStarted  += StartLoadingScreen;
        GameInstance.OnLoadFinished += FinishLoadingScreen;

        //UI & Camera disabled by default
        if (cam && loadingCanvas)
        {
            cam.enabled = false;
            loadingCanvas.enabled = false;
        }

        base.Awake();
    }

    protected override void OnDestroy()
    {
        GameInstance.OnLoadStarted  -= StartLoadingScreen;
        GameInstance.OnLoadFinished -= FinishLoadingScreen;
    }

    public void StartLoadingScreen()
    {
        Debug.Log("Loading");

        if (cam && loadingCanvas)
        {
            cam.enabled = true;
            loadingCanvas.enabled = true;
        }
    }

    public void FinishLoadingScreen()
    {
        Debug.Log("No Longer Loading");

        if (cam && loadingCanvas)
        {
            cam.enabled = false;
            loadingCanvas.enabled = false;
        }
    }
}
