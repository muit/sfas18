﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Crab;
using Crab.Utils;


public enum LocalGameWinner {
    None,
    Aborted,
    PlayerA,
    PlayerB,
    Tie
}

public class LocalGameScene : GameScene {

    public float roundDuration = 240; //4 minutes
    public float transitionDuration = 1;
    public int roundsToWin = 5;
    public List<PlayerInfo> playerInfos = new List<PlayerInfo>();
    public List<VehicleInfo> vehiclesByRound = new List<VehicleInfo>();

    [NonSerialized]
    public Delay finishTimer;
    Delay transitionTimer;
    SFASGameInstance gameInstance;
    bool roundPlaying = false;


    GameFinishReason finishReason = GameFinishReason.None;

    // Events
    public delegate void RoundStateDelegate();
    public event RoundStateDelegate OnRoundStart;
    public event RoundStateDelegate OnRoundFinish;

    protected override void OnGameStart(List<PlayerController> localPlayers)
    {
    }

    public override void StartMatch()
    {
        base.StartMatch();

        round = 0;

        gameInstance = SFASGameInstance.GetSFAS;
        finishTimer = new Delay(roundDuration, false);
        transitionTimer = new Delay(transitionDuration, false);

        StartRound();
    }

    protected override void Update()
    {
        if (finishTimer.Over())
        {
            FinishRound(GameFinishReason.TimePassed);
        }
        if (transitionTimer.Over()) {
            transitionTimer.Reset();

            PostFinishRound();
        }
    }

    void StartRound()
    {
        if (roundPlaying)
            return;

        roundPlaying = true;
        finishReason = GameFinishReason.None;

        gameInstance.OnStartRound();
        if (OnRoundStart != null)
            OnRoundStart();

        Cache.Get.camera.CleanTargets();

        //Spawn players
        foreach (PlayerInfo info in playerInfos)
        {
            if (info.spawner)
            {
                Entity prefab = GetPlayerVehicle(info).prefab;

                info.spawner.Activate(prefab);
            }
        }

        finishTimer.Start();
    }


    public void FinishRound(GameFinishReason reason)
    {
        //Don't allow finishing in transition state
        if (!roundPlaying || finishReason != GameFinishReason.None)
            return;

        SFASGameInstance.GetSFAS.OnFinishRound();

        finishReason = reason;

        if (finishReason != GameFinishReason.Aborted)
        {
            //Slow-mo transition
            transitionTimer.Start(transitionDuration);
            Time.timeScale = 0.3f;
        }
        else
        {
            PostFinishRound();
        }
    }

    private void PostFinishRound()
    {
        roundPlaying = false;

        round++;
        finishTimer.Reset();
        Time.timeScale = 1f;


        //Despawn players
        PlayerController[] oldPlayers = new PlayerController[players.Count];
        players.CopyTo(oldPlayers);

        foreach (PlayerController player in oldPlayers)
        {
            if (player)
            {
                player.Me.Despawn();
                // Force player unregister
                UnregistryPlayer(player);
            }
        }

        if(OnRoundFinish != null)
            OnRoundFinish();

        if (IsMatchCompleted() || finishReason == GameFinishReason.Aborted)
        {
            gameInstance.FinishMatch(finishReason);
        }
        else
        {
            StartRound();
        }
    }

    public override void PlayerDied(PlayerController player, Entity killer)
    {
        if (!IsRoundFinishing())
        {
            //Apply player scores
            PlayerController PlayerA = GetPlayer(0);
            PlayerController PlayerB = GetPlayer(1);
            if (PlayerA && PlayerB) {
                if (!PlayerA.Me.IsAlive())
                {
                    ++GetPlayerInfo(PlayerB).roundScore;
                }
                else if (!PlayerB.Me.IsAlive())
                {
                    ++GetPlayerInfo(PlayerA).roundScore;
                }
            }

            FinishRound(GameFinishReason.PlayerDied);
        }
    }
    
    public bool IsMatchCompleted()
    {
        return GetMatchWinner() != LocalGameWinner.None;
    }

    public LocalGameWinner GetMatchWinner()
    {
        if (finishReason == GameFinishReason.Aborted)
        {
            return LocalGameWinner.Aborted;
        }

        if (playerInfos.Count < 2)
            return LocalGameWinner.None;


        // 6-5 If a player won 6 rounds or more
        if (playerInfos[0].roundScore > roundsToWin)
            return LocalGameWinner.PlayerA;
        if (playerInfos[1].roundScore > roundsToWin)
            return LocalGameWinner.PlayerB;

        // 5-5 We should be entering Tie Round. No one won yet
        if (playerInfos[0].roundScore == roundsToWin && playerInfos[1].roundScore == roundsToWin)
            return LocalGameWinner.None;

        // 5-x
        if (playerInfos[0].roundScore == roundsToWin)
            return LocalGameWinner.PlayerA;

        // x-5
        if (playerInfos[1].roundScore == roundsToWin)
            return LocalGameWinner.PlayerB;

        return LocalGameWinner.None;
    }

    public bool IsMatchPointRound()
    {
        return !IsMatchCompleted() && playerInfos.Count >= 2 && (playerInfos[0].roundScore > roundsToWin || playerInfos[1].roundScore > roundsToWin);
    }

    public bool IsRoundFinishing()
    {
        return finishReason != GameFinishReason.None;
    }

    public VehicleInfo GetPlayerVehicle(PlayerInfo info)
    {
        if (info.roundScore < vehiclesByRound.Count)
        {
            return vehiclesByRound[info.roundScore];
        }
        else if (vehiclesByRound.Count > 0)
        {
            return vehiclesByRound[0];
        }
        return null;
    }

    public override PlayerInfo GetPlayerInfo(int playerId)
    {
        if (playerId >= playerInfos.Count)
            return null;

        return playerInfos[playerId];
    }

    public PlayerInfo GetPlayerInfo(PlayerController player)
    {
        int id = GetPlayerId(player);

        if (id < 0 || id >= playerInfos.Count)
            return null;
        return playerInfos[id];
    }
}
