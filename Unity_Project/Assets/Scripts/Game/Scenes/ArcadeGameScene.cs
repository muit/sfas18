﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using Crab;
using Crab.Utils;


public enum ArcadeGameWinner {
    None,
    Aborted,
    Player,
    AI,
    Tie
}

[System.Serializable]
public class ArcadeRoundInfo {
}

public class ArcadeGameScene : GameScene
{
    public float roundDuration = 240; //4 minutes
    public float transitionDuration = 1;
    public PlayerInfo playerInfo = new PlayerInfo();
    public List<VehicleInfo> vehicles = new List<VehicleInfo>();
    public List<ArcadeRoundInfo> rounds = new List<ArcadeRoundInfo>();

    public Delay specialVehicleTimer = new Delay(20, false);
    

    public VehicleInfo ActiveVehicle {
        get {
            if (playerInfo.activeVehicle == null) {
                playerInfo.activeVehicle = vehicles.First();
            }
            return playerInfo.activeVehicle;
        }
    }

    Delay transitionTimer;
    SFASGameInstance gameInstance;
    bool roundPlaying = false;

    GameFinishReason finishReason = GameFinishReason.None;


    // Events
    public delegate void RoundStateDelegate();
    public event RoundStateDelegate OnRoundStart;
    public event RoundStateDelegate OnRoundFinish;

    protected override void OnGameStart(List<PlayerController> localPlayers)
    {
    }

    public override void StartMatch()
    {
        base.StartMatch();

        round = 0;

        gameInstance = SFASGameInstance.GetSFAS;
        finishTimer = new Delay(roundDuration, false);
        transitionTimer = new Delay(transitionDuration, false);

        StartRound();
    }

    protected override void Update()
    {
        if (finishTimer.Over())
        {
            FinishRound(GameFinishReason.TimePassed);
        }
        if (transitionTimer.Over()) {
            transitionTimer.Reset();

            PostFinishRound();
        }

        if (specialVehicleTimer.Over()) {
            specialVehicleTimer.Reset();
            DeactivateSpecialVehicle();
        }
    }

    void StartRound()
    {
        if (roundPlaying)
            return;

        roundPlaying = true;
        finishReason = GameFinishReason.None;

        gameInstance.OnStartRound();
        if(OnRoundStart != null)
            OnRoundStart();

        Cache.Get.camera.CleanTargets();

        //Spawn player
        if (playerInfo.spawner)
        {
            Entity prefab = ActiveVehicle.prefab;

            playerInfo.spawner.Activate(prefab);
        }

        finishTimer.Start();
    }


    public void FinishRound(GameFinishReason reason)
    {
        //Don't allow finishing in transition state
        if (!roundPlaying || finishReason != GameFinishReason.None)
            return;

        //Reset active vehicle
        GetPlayerInfo(0).activeVehicle = vehicles[0];
        specialVehicleTimer.Reset();

        SFASGameInstance.GetSFAS.OnFinishRound();

        finishReason = reason;

        if (finishReason != GameFinishReason.Aborted)
        {
            //Slow-mo transition
            transitionTimer.Start(transitionDuration);
            Time.timeScale = 0.3f;
        }
        else
        {
            PostFinishRound();
        }
    }

    private void PostFinishRound()
    {
        roundPlaying = false;

        round++;
        finishTimer.Reset();
        Time.timeScale = 1f;

        if (OnRoundFinish != null)
            OnRoundFinish();

        if (IsMatchCompleted())
        {
            gameInstance.FinishMatch(finishReason);
        }
        else
        {
            // Kill AIs
            Entity[] worldEntities = FindObjectsOfType<Entity>();
            foreach (Entity entity in worldEntities)
            {
                if (entity)
                {
                    if (entity.IsAI())
                    {
                        entity.Despawn(0);
                    }
                }
            }

            //Despawn players
            {
                PlayerController[] oldPlayers = new PlayerController[players.Count];
                players.CopyTo(oldPlayers);

                foreach (PlayerController player in oldPlayers)
                {
                    if (player)
                    {
                        player.Me.Despawn();
                        // Force player unregister
                        UnregistryPlayer(player);
                    }
                }
            }

            StartRound();
        }
    }

    public override void PlayerDied(PlayerController player, Entity killer)
    {
        FinishRound(GameFinishReason.PlayerDied);
    }

    public void AIDied(Entity AI, Entity killer)
    {
        //If killed by the player
        if(killer.IsPlayer())
            playerInfo.score++;
    }

    public bool IsMatchCompleted()
    {
        return GetMatchWinner() != ArcadeGameWinner.None;
    }

    public ArcadeGameWinner GetMatchWinner()
    {
        if (finishReason == GameFinishReason.Aborted)
        {
            return ArcadeGameWinner.Aborted;
        }

        if (playerInfo == null || players.Count < 1)
            return ArcadeGameWinner.None;

        if (!GetPlayer(0).Me.IsAlive())
            return ArcadeGameWinner.AI;
        
        if (round >= rounds.Count)
            return ArcadeGameWinner.Player;
        
        return ArcadeGameWinner.None;
    }

    public bool IsRoundFinishing()
    {
        return finishReason != GameFinishReason.None;
    }

    public bool IsMatchRunning()
    {
        return round >= 0;
    }


    public VehicleInfo GetVehicle(string name)
    {
        return vehicles.Find((vehicle) => {
            return vehicle.name == name;
        });
    }

    public void ActivateSpecialVehicle(string vehicleName, float duration) {
        SetPlayerVehicle(0, vehicleName, false);
        specialVehicleTimer.Start(duration);
    }

    public void DeactivateSpecialVehicle()
    {
        SetPlayerVehicle(0, vehicles[0], false);
        specialVehicleTimer.Reset();
    }

    public bool SetPlayerVehicle(int playerId, string vehicleName, bool bResetPosition = false)
    {
        VehicleInfo vehicle = GetVehicle(vehicleName);
        return SetPlayerVehicle(playerId, vehicle, bResetPosition);
    }

    public bool SetPlayerVehicle(int playerId, VehicleInfo vehicle, bool bResetPosition = false)
    {
        if (vehicle != null)
        {
            PlayerController controller = GetPlayer(playerId);
            if (controller)
            {
                playerInfo.activeVehicle = vehicle;

                Entity entity = controller.Me;

                if (entity)
                {
                    PlayerInfo playerInfo = GetPlayerInfo(0);
                    Transform target = bResetPosition ? playerInfo.spawner.transform : entity.transform;

                    Entity temporalVehicle = SpawnEntity(transform, vehicle.prefab, target.position, target.rotation, false);
                    controller.SetControlledEntity(temporalVehicle);

                    if (entity)
                    {
                        entity.Despawn(0);
                    }
                }

                return true;
            }
        }
        return false;
    }

    public override PlayerInfo GetPlayerInfo(int playerId)
    {
        return playerInfo;
    }
}
