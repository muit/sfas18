﻿using System;
using UnityEngine;
using Crab;
using Crab.Utils;


public enum GameFinishReason
{
    None,
    TimePassed,
    AllEnemiesKilled,
    PlayerDied,
    Aborted
}

[Serializable]
public class PlayerInfo
{
    [NonSerialized]
    public int score;

    [NonSerialized]
    public int roundScore;

    public string name;
    public Color color;
    public PlayerSpawner spawner;

    [NonSerialized]
    public VehicleInfo activeVehicle;
}

[Serializable]
public class VehicleInfo
{
    public string name;
    public Entity prefab;
    public Sprite icon;
}


public class GameScene : SceneScript
{
    protected int round = -1;
    public int Round { get { return round; } }

    protected bool isMatchStarted = false;


    public delegate void ScoreDelegate();
    public event ScoreDelegate OnScoreChanged;

    [NonSerialized]
    public Delay finishTimer = new Delay(1, false);


    public virtual void StartMatch() {
        isMatchStarted = true;
    } 
    
    public virtual PlayerInfo GetPlayerInfo(int playerId)
    {
        return null;
    }

    public void JustChangedScore() {
        if (OnScoreChanged != null)
            OnScoreChanged();
    }
}
