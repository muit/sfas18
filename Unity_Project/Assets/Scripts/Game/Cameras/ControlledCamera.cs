﻿using UnityEngine;
using Crab.Entities;
using System.Collections.Generic;


[RequireComponent(typeof(Camera))]
public class ControlledCamera : BaseControlledCamera
{
	[Header("Split Camera")]
	public List<Transform> targets;

	public Vector3 targetOffset = new Vector3(0, 2.0f, 0);
	[Header("Distance")]
	public float zoom = 5.0f;
	public float maxZoom = 1000;
	public float minZoom = 5;
	public float zoomOffsetByDistance = 5;
	public float zoomByTargetDistance = 1;

	[Space(5)]
	public float zoomRate = 20;

	[Header("Speed")]
	public float xSpeed = 250.0f;
	public float ySpeed = 120.0f;
	[Space(5)]
	public float rotationDampening = 3.0f;
		

	[Header("Collision & Occlusion")]
	public LayerMask collidesWith;
	public float smoothCollisionRate = 15f;

    [Header("Lag")]
    public bool useCameraLag = true;
    public float lagSpeed = 10f;


    private float realZoom = 5.0f;
	private float expectedDistance = 5.0f;
	private float realDistance = 5.0f;
    Vector3 targetPosition;


    new private Camera camera;


	protected override void Start() {
		base.Start();

		// Make the rigid body not change rotation
		if (GetComponent<Rigidbody>())
			GetComponent<Rigidbody>().freezeRotation = true;
			
		camera = GetComponent<Camera>();


		realZoom = zoom;
	}

	void LateUpdate()
	{
		if (targets.Count <= 0)
			return;

		if (!isMouseLocked)
			return;

        //Clean invalid targets
        targets.Remove(null);
			
		Vector3 positionIncrement = Vector3.zero;

		for (int i = 0; i < targets.Count; i++)
		{
			Transform target = targets[i];
			if (!target)
				continue;

			positionIncrement += target.position;
		}

        // Camera Lag
        {
            if (!useCameraLag || targetPosition == Vector3.zero)
            {
                targetPosition = positionIncrement / targets.Count;
            }
            else
            {
                targetPosition = Vector3.Lerp(targetPosition, positionIncrement / targets.Count, Time.deltaTime * lagSpeed);
            }
        }
        
		float maximumDistanceToTarget = 0;
		for (int i = 0; i < targets.Count; i++)
		{
            Transform target = targets[i];
            if (target)
            {
                float distance = Vector3.Distance(targetPosition, target.position);
                if (maximumDistanceToTarget < distance)
                {
                    maximumDistanceToTarget = distance;
                }
            }
		}

		// Calculate optimal zoom
		zoom = zoomOffsetByDistance + zoomByTargetDistance * maximumDistanceToTarget;
		realZoom = Mathf.Lerp(realZoom, zoom, Time.deltaTime * zoomRate);
		realZoom = Mathf.Clamp(realZoom, minZoom, maxZoom);

		if (realZoom != 0)
		{
			expectedDistance = GetRayCastDistance(expectedDistance);
		}
		expectedDistance = Mathf.Clamp(expectedDistance, 0.0f, realZoom);

		if (realDistance < expectedDistance)
			realDistance = Mathf.Lerp(realDistance, expectedDistance, smoothCollisionRate * Time.deltaTime);
		else
			realDistance = expectedDistance;

		transform.position = targetPosition - (transform.rotation * Vector3.forward * (realDistance) - targetOffset);
	}

	static float ClampAngle(float angle, float min, float max) {
		if (angle < -360)
			angle += 360;
		if (angle > 360)
			angle -= 360;
		return Mathf.Clamp(angle, min, max);
	}

	private float GetRayCastDistance(float distance) {
		//RaycastHit hit;
		//Vector3 targetPos = target.position + targetOffset;

		Vector3[] corners = new Vector3[4];
		corners[0] = camera.ViewportToWorldPoint(new Vector3(1, 1, camera.nearClipPlane)) - transform.position;
		corners[1] = camera.ViewportToWorldPoint(new Vector3(0, 1, camera.nearClipPlane)) - transform.position;
		corners[2] = camera.ViewportToWorldPoint(new Vector3(0, 0, camera.nearClipPlane)) - transform.position;
		corners[3] = camera.ViewportToWorldPoint(new Vector3(1, 0, camera.nearClipPlane)) - transform.position;

		float HitDistance = zoom;

		foreach (Vector3 corner in corners)
		{
			//UnityEngine.Debug.DrawRay(targetPos + corner, transform.position - targetPos);
			/*if (Physics.Linecast(targetPos + corner, transform.position + corner, out hit, collidesWith))
			{
				//Find the closer collision
				if (hit.distance < HitDistance)
					HitDistance = hit.distance;
			}*/
		}

		if (HitDistance != zoom)
			return HitDistance;
		else if (collisionCount > 0)
			return distance;
		else
			return zoom;
	}


	int collisionCount = 0;

	void OnTriggerEnter(Collider col) {
		if(IsInLayerMask(col.gameObject, collidesWith))
			collisionCount++;
	}

	void OnTriggerExit(Collider col) {
		if (IsInLayerMask(col.gameObject, collidesWith))
			collisionCount = collisionCount > 0 ? collisionCount - 1 : 0;
	}

	private CBaseMovement GetMovement(Transform transform)
	{
		return transform.GetComponent<CBaseMovement>();
	}

	public override void AddTarget(Transform target)
	{
		if(!targets.Contains(target))
			targets.Add(target);
	}

	public override void RemoveTarget(Transform target)
	{
		targets.Remove(target);
	}

    public override Vector3 GetTargetPosition()
    {
        return targetPosition;
    }
}
