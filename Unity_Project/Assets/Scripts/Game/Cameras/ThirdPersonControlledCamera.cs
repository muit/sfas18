﻿using Crab;
using Crab.Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class ThirdPersonControlledCamera : BaseControlledCamera
{

    [Header("ThirdPerson Camera")]
    public Transform target;
    public Vector3 targetOffset = new Vector3(0, 2.0f, 0);

    [Header("Distance")]
    public float zoom = 5.0f;
    public float maxDistance = 20;
    public float minDistance = 2.5f;
    [Space(5)]
    public float zoomRate = 20;
    [Header("Speed")]
    public float xSpeed = 250.0f;
    public float ySpeed = 120.0f;
    [Space(5)]
    public float rotationDampening = 3.0f;
    [Header("Limit")]
    public float yMinLimit = -20;
    public float yMaxLimit = 80;

    [Header("Collision & Occlusion")]
    public LayerMask collidesWith;
    public float smoothCollisionRate = 15f;

    [Header("Lag")]
    public bool useCameraLag = true;
    public float lagSpeed = 10f;


    private float x = 0.0f;
    private float y = 0.0f;
    private float expectedDistance = 5.0f;
    private float realDistance = 5.0f;
    Vector3 targetPosition;

    private Entity targetEntity;
    private CBaseMovement targetMovement;
    

    protected override void Start()
    {
        base.Start();

        Vector3 angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;

        // Make the rigid body not change rotation
        if (GetComponent<Rigidbody>())
            GetComponent<Rigidbody>().freezeRotation = true;

        AddTarget(target);
    }

    void LateUpdate()
    {
        if (!target || (targetEntity && !targetEntity.IsAlive()))
            return;

        if (!isMouseLocked)
            return;

        // Mouse input
        {
            x += Input.GetAxis("Mouse X") * xSpeed * Time.deltaTime/*0.02f*/;
            y -= Input.GetAxis("Mouse Y") * ySpeed * Time.deltaTime/*0.02f*/;

            //Set Movement Direction
            Vector3 direction = transform.forward;
            direction.y = 0;

            if (targetMovement)
            {
                targetMovement.viewDirection = direction.normalized;
            }
        }


        y = ClampAngle(y, yMinLimit, yMaxLimit);
        transform.rotation = Quaternion.Euler(y, x, 0);


        zoom -= (Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime) * zoomRate * Mathf.Abs(zoom);
        zoom = Mathf.Clamp(zoom, minDistance, maxDistance);
        if (zoom != 0)
        {
            expectedDistance = GetRayCastDistance(zoom);
        }
        expectedDistance = Mathf.Clamp(expectedDistance, 0.0f, zoom);

        //Smooth distances if no collision is detected
        if (realDistance <= expectedDistance)
            realDistance = Mathf.Lerp(realDistance, expectedDistance, smoothCollisionRate * Time.deltaTime);
        else
            realDistance = expectedDistance;


        // Camera Lag
        {
            if (!useCameraLag || targetPosition == Vector3.zero)
            {
                targetPosition = target.position + targetOffset;
            }
            else
            {
                targetPosition = Vector3.Lerp(targetPosition, target.position + targetOffset, Time.deltaTime * lagSpeed);
            }
        }

        transform.position = targetPosition - (transform.forward * (realDistance)) + ApplyShakeOffset(transform.rotation); ;
    }

    static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360)
            angle += 360;
        if (angle > 360)
            angle -= 360;
        return Mathf.Clamp(angle, min, max);
    }

    private float GetRayCastDistance(float distance)
    {
        RaycastHit hit;
        Vector3 targetPos = target.position + targetOffset;

        Vector3 extents;
        extents = cam.ViewportToWorldPoint(new Vector3(1, 1, cam.nearClipPlane)) - transform.position;

        float HitDistance = distance;
        
        if (Physics.BoxCast(targetPos, extents, transform.position - targetPos, out hit, transform.rotation, distance, collidesWith))
        {
            Util.DrawBoxCastOnHit(targetPos, extents, transform.rotation, transform.position - targetPos, hit.distance, Color.red);

            //Find the closer collision
            if (hit.transform != target && hit.distance < HitDistance)
                HitDistance = hit.distance;
        }
        else
        {
            Util.DrawBoxCastBox(targetPos, extents, transform.rotation, transform.position - targetPos, distance, Color.green);
        }

        if (HitDistance != distance || collisionCount > 0)
            return HitDistance;
        else
            return zoom;
    }


    int collisionCount = 0;

    void OnTriggerEnter(Collider col)
    {
        if (IsInLayerMask(col.gameObject, collidesWith))
            collisionCount++;
    }

    void OnTriggerExit(Collider col)
    {
        if (IsInLayerMask(col.gameObject, collidesWith))
            collisionCount = collisionCount > 0 ? collisionCount - 1 : 0;
    }


    public override void AddTarget(Transform target)
    {
        if (target)
        {
            this.target = target;
            targetEntity = Entity.FindIn(this.target);
            if (targetEntity)
            {
                targetMovement = targetEntity.Movement;
            }

            //Look in the same direction than the target
            x = target.rotation.eulerAngles.y;
        }
    }

    public override void RemoveTarget(Transform target)
    {
        if (this.target == target)
        {
            this.target = null;
        }
    }

    public override void CleanTargets()
    {
        this.target = null;
    }

    public override Vector3 GetTargetPosition()
    {
        return target.position + targetOffset;
    }
}
