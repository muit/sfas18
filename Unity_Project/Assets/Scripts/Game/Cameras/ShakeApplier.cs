﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeApplier : MonoBehaviour {
    public float amount = 0.2f;
    public float duration = 0.3f;
    public float startTransitionDuration = 0.05f;
    public float endTransitionDuration = 0.2f;

    public bool applyOnStart = false;
    public bool debugApply = false;

    void Start () {
        if (applyOnStart)
            Apply();
	}

    private void Update()
    {
        if (debugApply)
        {
            Apply();
            debugApply = false;
        }
    }

    public void Apply() {
        if (Cache.Get.camera)
        {
            Cache.Get.camera.AddTrauma(amount, duration, startTransitionDuration, endTransitionDuration);
        }
    }
}
