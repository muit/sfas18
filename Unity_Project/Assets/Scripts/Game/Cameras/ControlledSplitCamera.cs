﻿using UnityEngine;

public class ControlledSplitCamera : BaseControlledCamera {

    [Header("Split Camera")]
	/*Reference both the transforms of the two players on screen.
	Necessary to find out their current positions.*/
	public Transform targetA;
	public Transform targetB;
    public Vector3 targetOffset = new Vector3(0, 2.0f, 0);

    [Header("Distance")]
    public float zoom = 10.0f;
    public float maxZoom = 1000;
    public float minZoom = 5;

    [Space(5)]
    public float zoomRate = 20;

    [Header("Speed")]
    public float xSpeed = 250.0f;
    public float ySpeed = 120.0f;
    [Space(5)]
    public float rotationDampening = 3.0f;

    [Header("Split Screen")]
    //The distance at which the split screen will be activated.
    public float splitDistance = 5;
    
	public Color splitterColor;
	public float splitterWidth = 0.01f;
    public float renderAreaSize = 2;
    

    private float realZoom = 5.0f;
    private float expectedDistance = 5.0f;
    private float realDistance = 5.0f;

    //The two cameras, both of which are initialized/referenced in the start function.
    private Camera camA;
	private Camera camB;

	//The two quads used to draw the second screen, both of which are initialized in the start function.
	private GameObject splitArea;
	private GameObject splitter;
    private GameObject splitLine;


    private Vector3 midPoint;
    private Vector3 midPointA;
    private Vector3 midPointB;


    protected override void Start ()
    {
        base.Start();

        // Make the rigid body not change rotation
        if (GetComponent<Rigidbody>())
            GetComponent<Rigidbody>().freezeRotation = true;

        realZoom = zoom;

        //Referencing camera A and initializing camera B.
        camA = GetComponent<Camera>();

        GameObject camBgo = new GameObject();
        camBgo.name = "Camera B";
        camBgo.transform.parent = transform.parent;
        camB = camBgo.AddComponent<Camera>();
		//Setting up the culling mask of cameraB to ignore the layer "TransparentFX" as to avoid rendering the split and splitter on both cameras.
		camB.cullingMask = ~(1 << LayerMask.NameToLayer ("TransparentFX"));


		//Setup up the splitter
		splitter = new GameObject();
        splitter.name = "Splitter";
		splitter.transform.parent = gameObject.transform;
		splitter.transform.localPosition = Vector3.forward * 0.7f;
		splitter.transform.localScale = new Vector3 (renderAreaSize, 1, 1);
		splitter.transform.localEulerAngles = Vector3.zero;
        splitter.SetActive(false);

        //Setup up the splitter's line
        splitLine = GameObject.CreatePrimitive(PrimitiveType.Quad);
        splitLine.name = "SplitLine";
        splitLine.transform.parent = splitter.transform;
        splitLine.transform.localPosition = Vector3.zero;
        splitLine.transform.localScale = new Vector3(1, splitterWidth, 1);
        splitLine.transform.localEulerAngles = Vector3.zero;

        //Setup up the splitter's draw rect
        splitArea = GameObject.CreatePrimitive (PrimitiveType.Quad);
        splitArea.name = "SplitArea";
        splitArea.transform.parent = splitter.transform;
		splitArea.transform.localPosition = new Vector3 (0, -(renderAreaSize/2), 0.001f);
		splitArea.transform.localScale = new Vector3 (1, renderAreaSize, 1);
		splitArea.transform.localEulerAngles = Vector3.zero;

        //Setup line and area render materials
        {
            Material lineMat = new Material(Shader.Find("Unlit/Color"));
            lineMat.color = splitterColor;
            splitLine.GetComponent<Renderer>().material = lineMat;
            splitLine.GetComponent<Renderer>().sortingOrder = 2;
            splitLine.layer = LayerMask.NameToLayer("TransparentFX");

            Material areaMat = new Material(Shader.Find("Mask/SplitScreen"));
            splitArea.GetComponent<Renderer>().material = areaMat;
            splitArea.layer = LayerMask.NameToLayer("TransparentFX");
        }
	}

	void LateUpdate ()
    {
        if (!targetA || !targetB)
            return;

        if (!isMouseLocked)
            return;

        Vector3 shakeOffset = ApplyShakeOffset(transform.rotation);


        float zDistance = targetA.position.z - targetB.position.z;
		float distance = Vector3.Distance (targetA.position, targetB.position);


        // Rotates the splitter according to the target
        float angle;
		if (targetA.position.x <= targetB.position.x) {
			angle = Mathf.Rad2Deg * Mathf.Acos (zDistance / distance);
		} else {
			angle = Mathf.Rad2Deg * Mathf.Asin (zDistance / distance) - 90;
		}
		splitter.transform.localEulerAngles = new Vector3 (0, 0, angle);


		// Midpoint between the two targets
		midPoint = (targetA.position + targetB.position) / 2;


        //Waits for the two cameras to split and then calculates a midpoint relevant to the difference in position between the two cameras.
        bool splitted = false;// distance > splitDistance;
        if (splitted)
        {
            Vector3 offsetA = midPoint - targetA.position;
            offsetA.x = Mathf.Clamp(offsetA.x, -splitDistance / 2, splitDistance / 2);
            offsetA.y = Mathf.Clamp(offsetA.y, -splitDistance / 2, splitDistance / 2);
            offsetA.z = Mathf.Clamp(offsetA.z, -splitDistance / 2, splitDistance / 2);
            midPointA = targetA.position + offsetA;

            Vector3 offsetB = midPoint - targetB.position;
            offsetB.x = Mathf.Clamp(offsetB.x, -splitDistance / 2, splitDistance / 2);
            offsetB.y = Mathf.Clamp(offsetB.y, -splitDistance / 2, splitDistance / 2);
            offsetB.z = Mathf.Clamp(offsetB.z, -splitDistance / 2, splitDistance / 2);
            midPointB = targetB.position + offsetB;


            if (splitter.activeSelf == false)
            {
                //Activate splitter and CamB once
                splitter.SetActive(true);
                camB.gameObject.SetActive(true);

                camB.transform.position = camA.transform.position;
                camB.transform.rotation = camA.transform.rotation;
            }
            else
            {
                UpdateCamera(camB, midPointB + shakeOffset);
            }

            UpdateCamera(camA, midPointA + shakeOffset);

            //Smooth line width
            float splittedCof = Mathf.Clamp01(distance - splitDistance + 0.5f);
            splitLine.transform.localScale = new Vector3(1, splittedCof * splitterWidth, 1);
        }
        else
        {
            if (splitter.activeSelf)
            {
                splitter.SetActive(false);
                camB.gameObject.SetActive(false);
            }

            UpdateCamera(camA, midPoint + shakeOffset);
        }
    }

    void UpdateCamera(Camera cam, Vector3 targetPosition)
    {
        cam.transform.position = targetPosition - (cam.transform.rotation * Vector3.forward * (realZoom) - targetOffset);
    }


    public override void AddTarget(Transform target)
    {
        if (!targetA || targetA == target)
        {
            targetA = target;
        }
        else if (!targetB || targetB == target)
        {
            targetB = target;
        }
    }

    public override void RemoveTarget(Transform target)
    {
        if (targetA == target)
        {
            targetA = null;
        }
        else if (targetB == target)
        {
            targetB = null;
        }
    }

    public override void CleanTargets() {
        targetA = null;
        targetB = null;
    }

    public override Vector3 GetTargetPosition()
    {
        return targetA.position + targetB.position / 2;
    }

    private void OnDrawGizmos()
    {
        Color lastColor = Gizmos.color;

        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(midPoint, 0.5f);
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(midPointA, 0.5f);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(midPointB, 0.5f);


        Gizmos.color = lastColor;
    }
}
