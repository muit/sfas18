﻿using Crab.Utils;
using System.Collections.Generic;
using UnityEngine;

class TraumaRegistry {

    public TraumaRegistry(float amount, float duration, float startTransitionDuration, float endTransitionDuration)
    {
        traumaToApply = amount;
        durationDelay = new Delay(duration, false);
        applyDelay = new Delay(startTransitionDuration, true);
        removeDelay = new Delay(endTransitionDuration, false);
    }
    
    float traumaToApply;
    Delay durationDelay;
    Delay applyDelay;
    Delay removeDelay;

    public float GetTraumaValue(out bool finished)
    {
        finished = false;

        if (durationDelay.IsStarted())
        {
            if (durationDelay.Over())
            {
                removeDelay.Start();
                durationDelay.Reset();
            }

            return traumaToApply;
        }
        else if (applyDelay.IsStarted())
        {
            float traumaApplied = traumaToApply * (1-applyDelay.CofRemaining());

            if (applyDelay.Over())
            {
                durationDelay.Start();
                applyDelay.Reset();
            }

            return traumaApplied;
        }
        else if (removeDelay.IsStarted())
        {
            float traumaApplied = traumaToApply * removeDelay.CofRemaining();

            if (removeDelay.Over())
            {
                finished = true;
            }

            return traumaApplied;
        }

        return 0;
    }
}


[RequireComponent(typeof(Camera))]
public class BaseControlledCamera : MonoBehaviour
{
    public bool isMouseLocked { get { return mouseLocked; } }
    private bool mouseLocked = true;

    [System.NonSerialized]
    public Camera cam;


    protected virtual void Start()
    {
        seed = Random.Range(0, int.MaxValue);
        trauma = traumaValue;
        
        cam = GetComponent<Camera>();
    }

    public virtual void AddTarget(Transform target)
    {}

    public virtual void RemoveTarget(Transform target)
    {}

    public virtual void CleanTargets()
    {}

    public void LockMouse(bool value)
    {
        mouseLocked = value;
        if (value)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }

    public static bool IsInLayerMask(GameObject obj, LayerMask mask)
    {
        return ((mask.value & (1 << obj.layer)) > 0);
    }

    public virtual Vector3 GetTargetPosition() {
        return transform.position;
    }


    /**
     * SHAKE
     */

    [Header("Shake")]
    [SerializeField]
    public float shakeSpeed = 7;

    [Range(0, 1)]
    public float traumaValue = 0;
    public float traumaSpeed = 12;
    public bool useExponentialTrauma = true;
    public Vector2 shakeAmplitude = Vector2.one;

    float expoTrauma { get { return trauma * trauma; } }

    float trauma;
    float lastTraumaTime = 0;
    int seed;
    List<TraumaRegistry> appliedTraumas = new List<TraumaRegistry>();
    List<TraumaRegistry> finishedTraumas = new List<TraumaRegistry>();


    public void AddTrauma(float amount, float duration = 0.3f, float startTransitionDuration = 0.05f, float endTransitionDuration = 0.1f) {
        appliedTraumas.Add(new TraumaRegistry(amount, duration, startTransitionDuration, endTransitionDuration));
    }
    public void SetTrauma(float value, bool instant = false)
    {
        traumaValue = Mathf.Clamp(value, 0, 1);

        if(instant)
            trauma = traumaValue;
    }

    public Vector3 ApplyShakeOffset(Quaternion rotation)
    {
        if (lastTraumaTime <= 0)
        {
            lastTraumaTime = Time.time;
        }
        float currentTime = Time.time;

        float stressDeltaTime = currentTime - lastTraumaTime;

        float currentTraumaValue = traumaValue;

        //Update trauma value
        {
            //Apply base trauma
            foreach (TraumaRegistry registry in appliedTraumas)
            {
                bool finished = false;
                currentTraumaValue += registry.GetTraumaValue(out finished);

                if (finished) {
                    //If effect finished, registry for removal
                    finishedTraumas.Add(registry);
                }
            }

            //Apply trauma effects
            foreach (TraumaRegistry registry in finishedTraumas)
            {
                appliedTraumas.Remove(registry);
            }
            finishedTraumas.Clear();
            
            //Trauma cant be more than 1
            currentTraumaValue = Mathf.Clamp(currentTraumaValue, 0, 1);
        }

        // Smoothly updates real trauma value
        if (trauma != currentTraumaValue) {
            trauma = Mathf.Lerp(trauma, currentTraumaValue, traumaSpeed * stressDeltaTime);
        }
        
        Vector3 offset = Vector3.zero;
        offset.x += expoTrauma * (-shakeAmplitude.x + shakeAmplitude.x * PerlinNoise(seed,     currentTime * shakeSpeed, 0.0f) * 2);
        offset.y += expoTrauma * (-shakeAmplitude.x + shakeAmplitude.x * PerlinNoise(seed + 1, currentTime * shakeSpeed, 0.0f) * 2);

        //Apply offset in parallel to camera rotation
        offset = rotation * offset;

        lastTraumaTime = currentTime;
        return offset;
    }


    float PerlinNoise(int seed, float x, float y)
    {
        // Generate a new seed to make sure it is randomized
        Random.State lastState = Random.state;
        Random.InitState(seed);
        float newSeed = (float)Random.Range(0, int.MaxValue) / 1000;
        Random.state = lastState;
        
        float value = Mathf.PerlinNoise(newSeed + x, newSeed + y);
        return value;
    }
}
