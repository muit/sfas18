﻿using Crab;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    // --------------------------------------------------------------

    [SerializeField]
    Text m_PlayerAScoreText;
    [SerializeField]
    Text m_PlayerARoundScoreText;
    [SerializeField]
    Image m_PlayerAVehicle;

    [Space(10)]

    [SerializeField]
    Text m_PlayerBScoreText;
    [SerializeField]
    Text m_PlayerBRoundScoreText;
    [SerializeField]
    Image m_PlayerBVehicle;

    [Space(10)]

    [SerializeField]
    Text m_Rounds;

    [SerializeField]
    Text m_Time;

    [Space(10)]


    [SerializeField]
    RectTransform m_GameOverPanel;
    [SerializeField]
    Text m_GameOverWinner;

    // --------------------------------------------------------------

    SFASGameInstance gi;
    LocalGameScene scene;
    

    private void Start()
    {
        gi = SFASGameInstance.GetSFAS;

        scene = SceneScript.Get<LocalGameScene>(this);
        scene.OnScoreChanged += OnUpdateScore;
        scene.OnRoundFinish += OnUpdateScore;
        OnUpdateScore();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            scene.FinishRound(GameFinishReason.Aborted);
        }

        float remaining = Mathf.Floor(scene.finishTimer.TimeRemaining());
        float seconds = remaining % 60;
        float minutes = (remaining - seconds) / 60;
        m_Time.text = minutes.ToString() + ":" + seconds.ToString("00");
    }

    void OnDisable()
    {
        scene.OnScoreChanged -= OnUpdateScore;
        scene.OnRoundFinish -= OnUpdateScore;
    }

    void OnUpdateScore()
    {
        m_Rounds.text = "Round " + scene.Round;

        PlayerInfo playerA = scene.GetPlayerInfo(0);
        PlayerInfo playerB = scene.GetPlayerInfo(1);
    
        m_PlayerAScoreText.color      = playerA.color;
        m_PlayerAScoreText.text       = "" + playerA.score;
        m_PlayerARoundScoreText.color = playerA.color;
        m_PlayerARoundScoreText.text  = playerA.roundScore + "/" + scene.roundsToWin;

        m_PlayerBScoreText.color      = playerB.color;
        m_PlayerBScoreText.text       = "" + playerB.score;
        m_PlayerBRoundScoreText.color = playerB.color;
        m_PlayerBRoundScoreText.text  = playerB.roundScore + "/" + scene.roundsToWin;

        m_PlayerAVehicle.sprite = scene.GetPlayerVehicle(playerA).icon;

        m_PlayerBVehicle.sprite = scene.GetPlayerVehicle(playerB).icon;


        bool gameFinished = scene.IsMatchCompleted();
        m_GameOverPanel.gameObject.SetActive(gameFinished);
        Cache.Get.camera.LockMouse(!gameFinished);

        if (scene.GetMatchWinner() == LocalGameWinner.PlayerA)
        {
            m_GameOverWinner.color = scene.GetPlayerInfo(0).color;
            m_GameOverWinner.text = "Red won!";
        }
        else if (scene.GetMatchWinner() == LocalGameWinner.PlayerB)
        {
            m_GameOverWinner.color = scene.GetPlayerInfo(1).color;
            m_GameOverWinner.text = "Blue won!";
        }
        else if (scene.GetMatchWinner() == LocalGameWinner.Aborted)
        {
            m_GameOverWinner.text = "";
        }
    }
    public void PlayAgain()
    {
        gi.StartGame(GameMode.Local, true);
    }

    public void GoMenu()
    {
        gi.GoMenu();
    }
}
