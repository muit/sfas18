﻿using Crab;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIArcadeManager : MonoBehaviour
{

    // --------------------------------------------------------------

    [SerializeField]
    Text m_PlayerScore;
    [SerializeField]
    Text m_PlayerScoreLabel;
    [SerializeField]
    Image m_PlayerVehicle;

    [Space(10)]

    [SerializeField]
    Text m_Rounds;

    [SerializeField]
    Text m_Time;

    [SerializeField]
    Text m_SpecialTime;

    [Space(10)]


    [SerializeField]
    RectTransform m_GameOverPanel;
    [SerializeField]
    Text m_GameOverScore;

    // --------------------------------------------------------------

    SFASGameInstance gi;
    ArcadeGameScene scene;


    private void Start()
    {
        gi = SFASGameInstance.GetSFAS;

        scene = SceneScript.Get<ArcadeGameScene>(this);
        scene.OnRoundStart += OnUpdateScore;
        scene.OnRoundFinish += OnUpdateScore;
        OnUpdateScore();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            scene.FinishRound(GameFinishReason.Aborted);
        }

        float remaining = Mathf.Floor(scene.finishTimer.TimeRemaining());
        float seconds = remaining % 60;
        float minutes = (remaining - seconds) / 60;
        m_Time.text = minutes.ToString() + ":" + seconds.ToString("00");

        m_PlayerVehicle.sprite = scene.ActiveVehicle.icon;

        PlayerInfo player = scene.GetPlayerInfo(0);
        m_PlayerScore.text = "" + player.score;
        m_GameOverScore.text = "" + player.score; 

        if (scene.specialVehicleTimer.IsStarted())
        {
            m_SpecialTime.enabled = true;

            remaining = Mathf.Ceil(scene.specialVehicleTimer.TimeRemaining());
            seconds = remaining % 60;
            m_SpecialTime.text = seconds.ToString();
        }
        else
        {
            m_SpecialTime.enabled = false;
        }
        

        bool gameFinished = scene.IsMatchCompleted();
        Cache.Get.camera.LockMouse(!gameFinished);
    }

    void OnDisable()
    {
        scene.OnRoundStart -= OnUpdateScore;
        scene.OnRoundFinish -= OnUpdateScore;
    }

    void OnUpdateScore()
    {
        m_Rounds.text = "Horde " + scene.Round;


        PlayerInfo player = scene.GetPlayerInfo(0);

        m_PlayerScore.color = player.color;
        m_PlayerScoreLabel.color = player.color;


        bool gameFinished = scene.IsMatchCompleted();
        m_GameOverPanel.gameObject.SetActive(gameFinished);
        Cache.Get.camera.LockMouse(!gameFinished);

        if (scene.GetMatchWinner() == ArcadeGameWinner.Aborted)
        {
            m_GameOverScore.text = "";
        }
    }


    public void PlayAgain() {
        gi.StartGame(GameMode.Arcade, true);
    }

    public void GoMenu() {
        gi.GoMenu();
    }
}
