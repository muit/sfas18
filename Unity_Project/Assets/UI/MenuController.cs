﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {
    public void Play() {
        SFASGameInstance.GetSFAS.StartGame();
    }

    public void Exit() {
        Application.Quit();
    }
}
