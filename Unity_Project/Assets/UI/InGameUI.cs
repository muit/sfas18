﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Crab;

public class InGameUI : MonoBehaviour {

    public Text health;
    public Text level;
    public Text timeRemaining;
    SFASGameInstance gameInstance;
    LocalGameScene scene;

    // Use this for initialization
    void Start () {
        gameInstance = SFASGameInstance.GetSFAS;
        scene = SceneScript.Get<LocalGameScene>(this);
    }
	
	// Update is called once per frame
	void Update () {
        Entity player = SceneScript.Get(this).GetPlayer(0).Me;

        if (player) {
            health.text = ""+player.Attributes.Live;
        }
        
        level.text = "Round " + (scene.Round+1);

        timeRemaining.text = scene.finishTimer.TimeRemaining().ToString("F1");
    }
}
