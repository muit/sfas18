﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(AudioSource))]
public class EngineSound : MonoBehaviour
{
    public AudioClip engineStartClip;
    public AudioClip engineLoopClip;

    AudioSource source;

    void Start()
    {
        source = GetComponent<AudioSource>();
        if (source)
        {
            source.loop = true;
            StartCoroutine(playEngineSound());
        }
    }

    IEnumerator playEngineSound()
    {
        source.clip = engineStartClip;
        source.Play();
        yield return new WaitForSeconds(source.clip.length);
        source.clip = engineLoopClip;
        source.Play();
    }
}
