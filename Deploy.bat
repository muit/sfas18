@echo off
setlocal EnableDelayedExpansion

echo.
echo Login on Itchio...
echo.
"Itch.io/butler" login
echo.
echo Deploying Build from "%cd%/Playable_Demo"...
echo.
"Unity_Project/Itch.io/butler" push %cd%/Playable_Demo muit/remain-in-conflict:windows-beta

:exit
pause>nul
exit
